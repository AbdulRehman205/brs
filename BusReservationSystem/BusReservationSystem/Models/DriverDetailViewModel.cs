﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusReservationSystem.Models
{
    public class DriverDetailViewModel
    {
        public int DriverId { get; set; }
        public string RouteName { get; set; }
        public string DriverName { get; set; }
        public int MobileNumber { get; set; }
        public string LicenceNumber { get; set; }
        public Nullable<int> AlternateNumber { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
  
    }
}