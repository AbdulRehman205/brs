﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusReservationSystem.Models
{
    public class TicketFareViewModel
    {
        public int TicketFareId { get; set; }
        public string ArrivalStation { get; set; }
        public string DepartureStation { get; set; }
        public decimal Distance { get; set; }
        public decimal Fare { get; set; }
        public string RouteName { get; set; }
        public Nullable<System.DateTime> FareDate { get; set; }
        public string TicketType { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
   
    }
}