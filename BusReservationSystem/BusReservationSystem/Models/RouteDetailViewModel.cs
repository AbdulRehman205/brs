﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusReservationSystem.Models
{
    public class RouteDetailViewModel
    {
        public RouteDetailViewModel()
        {
            this.DriverDetails = new List<DriverDetailViewModel>();
            this.TicketFares = new List<TicketFareViewModel>();
            this.TimeTables = new List<TimeTableViewModel>();
        }

        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public string Arrival { get; set; }
        public string Departure { get; set; }
        public Nullable<decimal> Distance { get; set; }
        public string ViaStation { get; set; }
        public string BusNumber { get; set; }
        public string BusPlateNumber { get; set; }
        public string BusType { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
       
    
public  List<TimeTableViewModel> TimeTables { get; set; }
public  List<TicketFareViewModel> TicketFares { get; set; }
public  List<DriverDetailViewModel> DriverDetails { get; set; }}
}