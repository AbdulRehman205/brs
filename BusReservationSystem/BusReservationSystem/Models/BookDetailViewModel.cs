﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusReservationSystem.Models
{
    public class BookDetailViewModel
    {
        public int BookingId { get; set; }
        public int PassengerId { get; set; }
        public string ArrivalStation { get; set; }
        public System.DateTime BookingDate { get; set; }
        public string BusType { get; set; }
        public string DepartureStation { get; set; }
        public decimal Distance { get; set; }
        public string BoardingPoint { get; set; }
        public System.DateTime JourneyDate { get; set; }
        public string SMSStatus { get; set; }
        public string EmailStatus { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public decimal Fare { get; set; }
        public Nullable<decimal> AditionalFare { get; set; }
        public decimal GST { get; set; }
        public Nullable<decimal> ServiceCharge { get; set; }
        public decimal Commision { get; set; }
        public string RouteName { get; set; }
        public string SeatNumber { get; set; }
        public string ViaStation { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}