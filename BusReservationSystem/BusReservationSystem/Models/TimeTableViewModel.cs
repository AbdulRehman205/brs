﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusReservationSystem.Models
{
    public class TimeTableViewModel
    {
        public int TimeTableId { get; set; }
        public string ArrivalStation { get; set; }
        public System.DateTime ArrivalTime { get; set; }
        public string DepartureStation { get; set; }
        public System.DateTime DeparturreTime { get; set; }
        public int Distance { get; set; }
        public Nullable<decimal> TotalJourneyTime { get; set; }
        public decimal Fare { get; set; }
        public string RouteName { get; set; }
        public string DelayInformation { get; set; }
        public Nullable<int> Mobile { get; set; }
        public string ViaStation { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
 
    }
}