﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusReservationSystem.Models
{
    public class PassengerDetailViewModel
    {
        public PassengerDetailViewModel()
        {
            this.BookDetails = new List<BookDetailViewModel>();
        }

        public int PassengerId { get; set; }
        public string PassengerName { get; set; }
        public int Mobile { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
  
    
public  List<BookDetailViewModel> BookDetails { get; set; }}
}