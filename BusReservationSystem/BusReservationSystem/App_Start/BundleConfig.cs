﻿using System.Web;
using System.Web.Optimization;

namespace BusReservationSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/angular-material.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(

                "~/Scripts/Angular/angular.js",
               "~/Scripts/Angular/angular-animate.js",
               "~/Scripts/angular-route.min.js",
               "~/Scripts/Angular/angular-aria.js",
               "~/Scripts/Angular/angular-messages.min.js",
               "~/Scripts/AngularMaterial/angular-material.js",
               "~/Scripts/app/app.js",
               "~/Scripts/app/app.config.js",
               "~/Scripts/app/Home/home.controller.js"

               ));
            System.Web.Optimization.BundleTable.EnableOptimizations = true;
        }
    }
}
