﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System.Configuration;
//Mvcusing Unity.AspNet.WebApi;
using Microsoft.Practices.Unity.Mvc;
using System.Web.Mvc;

namespace BusReservationSystem.App_Start
{
    public class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            var unityConfigurationSection = ConfigurationManager.GetSection("unity") as UnityConfigurationSection;
            // container.RegisterType(typeof(IRepository<>), typeof(Repository<>));
            if (unityConfigurationSection != null)
                unityConfigurationSection.Configure(container);

            // for mvc5
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            // for webapi
           // GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}