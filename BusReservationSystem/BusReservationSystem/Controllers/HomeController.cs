﻿using BusinessLogicLayer.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BusReservationSystem.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        private readonly IUserLoginService _userLoginService;
        public HomeController(IUserLoginService userLoginService)
        {
            // sample for demo to call service 
            _userLoginService = userLoginService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}