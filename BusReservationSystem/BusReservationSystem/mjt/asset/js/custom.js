/*!
 * custom js

 1. Menu 
 */

/****************************1.  custom js for dropdown menu *****************************************/

;(function($) {
 
     // DOM ready
     $(function() {
          $('.nav-list > li').addClass('nav-item');
           
          // Append the mobile icon nav
          $('.nav').prepend($('<div class="nav-mobile">Menu</div>'));
           
          // Add a <span> to every .nav-item that has a <ul> inside
          $('.nav-item').has('ul').prepend('<span class="nav-click"><i class="fa fa-bars"></i></span>');
           
          // Click to reveal the nav
          $('.nav-mobile').click(function(){
               if($('.nav-list').is(':hidden')){
               $('.nav-mobile').addClass("open");
            $('.nav-list').slideDown( "slow", function() {
               });
               }else{
               $('.nav-mobile').removeClass("open");
               $('#lk').removeClass("vis");
               $('.nav-list').slideUp( "slow", function() {
               });}
          });
           
          // Dynamic binding to on 'click'
          $('.nav-list').on('click', '.nav-click', function(){
           
               // Toggle the nested nav
               /*$(this).siblings('.sub-menu').toggle();*/
               if($(this).siblings('.sub-menu').is(':hidden')){
            $(this).siblings('.sub-menu').slideDown( "slow", function() {
               });
               }else{$(this).siblings('.sub-menu').slideUp( "slow", function() {
               // Animation complete.
               });}
                
               // Toggle the arrow using CSS3 transforms
               $(this).children('.nav-arrow').toggleClass('nav-rotate');
                
          });
          
     });
      
})(jQuery);



/******************************************                        ***************************************/
