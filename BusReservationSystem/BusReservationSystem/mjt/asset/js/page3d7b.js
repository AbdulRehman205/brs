var loading_big = "<img src='" + base_url + "asset/images/19.gif' />";
$(document).ready(function() {    
    //  init date picker
    $("#srch_date").datepicker({
        todayHighlight: true,
        numberOfMonths:2,
        format: "dd/mm/yyyy",
        startDate: '-0m',
        autoclose: true
    }).on('changeDate', function(e) {
        var fDate = new Date(e.date);
        var end = new Date(fDate.setDate(fDate.getDate() + 365));
        $('#return_date').datepicker('setStartDate', e.date);
        $("#return_date").datepicker("setEndDate", end)
    });
    
    $('#return_date').datepicker({
        todayHighlight: true,
        startDate: '-0m',
        format: "dd/mm/yyyy",
        maxDate: "+365D",
        autoclose: true
    }).on('changeDate', function(e) {
        $('#srch_date').datepicker('setEndDate', e.date)
    });
    
    $('#register-section').hide();
    $('#forget-section').hide();
    
    $('#toregister').click(function(e) {
        e.preventDefault();
        $('#login-section').fadeOut('slow', function() {
            $('#register-section').fadeIn('slow');
            $('#r_name').focus();
        });        
    });
    
    $('#tologin').click(function(e) {
        e.preventDefault();
        $('#register-section').fadeOut('slow', function() {
            $('#login-section').fadeIn('slow');
            $('#user_id').focus();
        });        
    });
    
    $('#toforget').click(function(e) {
        e.preventDefault();
        $('#login-section').fadeOut('slow', function() {
            $('#forget-section').fadeIn('slow');
            $('#f_email').focus();
        });        
    });
    
    $('#tologinfromforget').click(function(e) {
        e.preventDefault();
        $('#forget-section').fadeOut('slow', function() {
            $('#login-section').fadeIn('slow');
            $('#user_id').focus();
        });        
    });
    
   $(document.body).on("click", ".ui-widget-overlay", function()    {
        $.each($(".ui-dialog"), function()        {
            var $dialog;
            $dialog = $(this).children(".ui-dialog-content");
            if($dialog.dialog("option", "modal"))            {
                $dialog.dialog("close");
            }
        });
    });
    
    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

});

function fire_action(e, type) {
    if (e.which == 13) {  // detect the enter key
        if(type==1) init_login();
        if(type==2) init_register();
        if(type==3) init_forgotpass();
    }
}

function activate_login(){
    $('#register-section').hide();
    $('#forget-section').hide(); 
    $('#login-section').show(); 
    window.setTimeout(function() {
        $('#user_id').focus();
    }, 1000);
            
}

function activate_register(){
    $('#register-section').show();
    $('#forget-section').hide(); 
    $('#login-section').hide();
    window.setTimeout(function() {
        $('#r_name').focus();
    }, 1000);
}

function init_login(){
    $('#d_error').html("");
    if ($("#user_id").val() == "") {
        $('#d_error').html("Please enter username or email");
        $("#user_id").focus();
        return false;
    } else if ($("#pwd_id").val() == "") {
        $('#d_error').html("Please enter password");
        $("#pwd_id").focus();
        return false;
    }
    var data = {};
    data.user = $("#user_id").val();
    data.pass = $("#pwd_id").val();
    $("#d_error").html("Please wait...");
    $('#login-btn').attr('disabled', 'disabled');
    $.ajax({
        type: "POST",
        url: base_url + "login/authenticate",
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status == 1) {
                $("#d_error").html("Login success");
                //if (result.type == 'CUST')
                    window.location.reload();
                //else
                    //window.location.href = base_url + 'login/dashboard';
            } else {
                 $('#login-btn').removeAttr('disabled');
                $("#d_error").html("Invalid username or password");
            }
        }
    });
}

function init_register(){
    $('#r_error').html("");
    var data = {};
    data.email = $.trim($("#r_username").val());
    data.pass = $.trim($("#r_pass").val());
    data.name = $.trim($("#r_name").val());
    data.mobile = $.trim($("#r_mobile").val());
    
    if (data.name == "") {
        $('#r_error').html("Please enter name");
        $("#r_cpass").focus();
        return false;
    } else if (data.email == "") {
        $('#r_error').html("Please enter email");
        $("#r_username").focus();
        return false;
    } else if (!is_email(data.email)) {
        $('#r_error').html("Please enter valid email");
        $("#r_username").focus();
        return false;
    } else if (data.pass == "") {
        $('#r_error').html("Please enter password");
        $("#r_pass").focus();
        return false;
    } else if (data.pass.length < 6) {
        $('#r_error').html("Password should be minimum 6 charactors");
        $("#r_pass").focus();
        return false;
    } else if (data.mobile == "") {
        $('#r_error').html("Please enter mobile");
        $("#r_mobile").focus();
        return false;
    } else if (!is_phone( data.mobile )) {
        $('#r_error').html("Please enter value mobile");
        $("#r_mobile").focus();
        return false;
    }
    
    $("#r_error").html("Please wait...");
    $('#register-button').attr('disabled', 'disabled');
    $.ajax({
        type: "POST",
        url: base_url + "account/register",
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status == 1) {
                $("#r_error").html("Your account has been created");
                window.location.reload();
            } else if (result.status == 0){
                $('#register-button').removeAttr('disabled');
                $("#r_error").html("Email already registered. Please try with different email");
            } else if (result.status == 3){
                $('#register-button').removeAttr('disabled');
                $("#r_error").html("Sorry! Problem in creating account. Please try again later");
            }
        }
    });
}

function editProfile(code) {
    var data = {};
    data.name = $('#username').val();
    data.fname = $('#fname').val();
    data.email = $('#email').val();
    data.mobile = $('#mobile').val();
    data.code = code;
    $("#usr-edit-states").html("");
    if (data.name == "") {
        $('#usr-edit-states').html("Please enter name");
        $("#r_cpass").focus();
        return false;
    } else if (data.email == "") {
        $('#usr-edit-states').html("Please enter email");
        $("#r_username").focus();
        return false;
    } else if (!is_email(data.email)) {
        $('#usr-edit-states').html("Please enter valid email");
        $("#r_username").focus();
        return false;
    } else if (data.mobile == "") {
        $('#usr-edit-states').html("Please enter mobile");
        $("#r_mobile").focus();
        return false;
    } else if (!is_phone( data.mobile )) {
        $('#usr-edit-states').html("Please enter value mobile");
        $("#r_mobile").focus();
        return false;
    }
    
    $("#usr-edit-states").html("Please wait...");
    $('#btn-edit-profile').attr('disabled', 'disabled');
    $.ajax({
        type: "POST",
        url: base_url + "account/edit-profile",
        data: data,
        dataType: 'json',
        success: function(result) {
            $('#btn-edit-profile').removeAttr('disabled');
            if (result.status == 1) {
                $("#usr-edit-states").html("Your details are updated successfully");
                window.setTimeout(function() {
                    $("#usr-edit-states").html("");
                }, 3000);
            } else if (result.status == 0){                
                $("#usr-edit-states").html("Problem in updating your details. Please try again later");
            } 
        }
    });
}

function changePassword() {
    var data = {};
    data.oldpassword = $.trim( $('#old-password').val() );
    data.newpassword = $.trim( $('#new-password').val() );
    data.confirm = $.trim( $('#confirm-password').val() );
    var err = 0;
    if (data.oldpassword == '') {
        $('#pwd-action-state').html("Please enter current password");
        $("#old-password").focus();
        return false;
    } else if (data.newpassword == '') {
        $('#pwd-action-state').html("Please enter new password");
        $("#new-password").focus();
        return false;
    } else if (data.newpassword.length < 6) {
        $('#pwd-action-state').html("New password should be minimum 6 charactors");
        $("#new-password").focus();
        return false;
    } else if (data.confirm == '') {
        $('#pwd-action-state').html("Please enter confirm password");
        $("#confirm-password").focus();
        return false;
    } else if (data.newpassword != data.confirm) {
        $('#pwd-action-state').html("New password & Confirm password must be same");
        $("#confirm-password").focus();
        return false;
    } 
    
    $('#pwd-form-bttons').attr('disabled', 'disabled');
    $('#pwd-action-state').html("Please wait...");
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: base_url + "account/change-password",
        data: data,
        success: function (result) {
            $('#pwd-form-bttons').removeAttr('disabled');
            if (result.status == 1) {
                $('#pwd-action-state').html("Your password updated successfully");
                window.setTimeout(function() {
                    $("#pwd-action-state").html("");
                }, 3000);
            } else if (result.status == 0){                
                $("#pwd-action-state").html(result.errorDesc);
            } 
        }
    });

}

function set_to_station(){
    //  load to stations
    if( $("#frm_id").val() != "" ){
       var from_code = $("#frm_id").val();
       var to_codes = eval(routes[from_code]);
       var html="<option selected='selected' value=''>Select Destination</option>";
       $(to_codes).each(function() {
           html+="<option value="+this.code+">"+this.name+"</option>";
       });
       $('#to_id').html(html);
    }

}

function init_search(){
    // init search     
    $("#errorMsg2").html("");
    if ($("#frm_id").val() == ""){
        $("#errorMsg2").html("Please select origin");
        $("#frm_id").focus();
        return false;
    } else if ($("#to_id").val() == "") {
        $("#errorMsg2").html("Please select destination");
        $("#errorMsg2").show(1000);
        $("#to_id").focus();
        return false;
    } else if ($("#srch_date").val() == "") {
        $("#errorMsg2").html("Please journey date");
        $("#srch_date").focus();
        return false;
    }   
    $('#btn-search').val('Searching...');
    var url = base_url+"search?fromCityCode="+$("#frm_id").val()+"&toCityCode="+$("#to_id").val()+"&tripDate="+$("#srch_date").val()+"&returnDate="+$("#return_date").val()+"&type=1";
    $(location).attr("href", url);
}

function toggle_modify_search() {
    $('.trip-details').toggle();
    $('.modify-search').toggle();    
}

function search_to(opt) {
    var new_date = prev_date;
    if(opt=='next') new_date = next_date;
    var url = base_url+"search?fromCityCode="+from_code+"&toCityCode="+to_code+"&tripDate="+new_date+"&returnDate="+return_date+"&type=1";
    $(location).attr("href", url);
}

function modify_search() {
    if ($("#frm_id").val() == ""){
        $("#errorMsg2").html("Please select from city</p>");
        $("#frm_id").focus();
        return false;
    } else if ($("#to_id").val() == "") {
        $("#errorMsg2").html("Please select to city");
        $("#errorMsg2").show(1000);
        $("#to_id").focus();
        return false;
    } else if ($("#search_date").val() == "") {
        $("#errorMsg2").html("Please travel date</p>");
        $("#search_date").focus();
        return false;
    }  

    var url = base_url+"search?fromCityCode="+$("#frm_id").val()+"&toCityCode="+$("#to_id").val()+"&tripDate="+$("#search_date").val();
    $(location).attr("href", url);
}

function init_trip_search(from_code, to_code, search_date, return_date, type) {
    var data = {};
    data.fromCityCode = from_code;
    data.toCityCode = to_code;
    data.tripDate = search_date;
    data.returnDate = return_date;
    data.searchType = type;    
    $("#search-result-container").html('<br/><br/><div class="text-center">'+loading_big+'</div>');
    $.ajax({
        type: "POST",
        url: base_url + "search/search-list",
        data: data,
        dataType: 'html',
        success: function(result) {
            $('#search-result-container').html(result);            
            init_trip_details();
        }
    });
}

function init_trip_details() {
    if( $(trips_json).length>0 ) {
        $.each(trips_json, function(key, trip) {
            var tripcode = trip.tripCode;
            var boarding = '<table width="200"><tr> <th colspan="2">Boarding Points</th> </tr>';
            $.each(trip.fromStation.stationPoint, function(pbkey, bp) {
                boarding += '<tr> <td width="65%">'+bp.name+'</td>    <td width="35%">'+dateToDateTime(bp.dateTime,2)+'</td>';
            });
            
            var arrival = '<table width="200"><tr> <th colspan="2">Dropping Points</th> </tr>';
            $.each(trip.toStation.stationPoint, function(pbkey, dp) {
                arrival += '<tr> <td width="65%">'+dp.name+'</td>    <td width="35%">'+dateToDateTime(dp.dateTime,2)+'</td>';
            });
            
            var via_stations = '<table width="200"><tr> <th colspan="2">Via Stations</th> </tr>';
            $.each(trip.viaStations, function(pbkey, vs) {
                via_stations += '<tr> <td width="95%">'+vs.name+'</td>';
            });
            
            $('#trip_'+tripcode).find('.boarding').popover({
                container:'body',
                content:boarding,
                html:true,
                placement:'right',
                trigger:'hover',
            });
            
            $('#trip_'+tripcode).find('.arrival').popover({
                container:'body',
                content:arrival,
                html:true,
                placement:'right',
                trigger:'hover',
            });
            
            $('#trip_'+tripcode).find('.more_via').popover({
                container:'body',
                content:via_stations,
                html:true,
                placement:'right',
                trigger:'hover',
            });
            
        });
    }
}

function close_info_popup() {
    $('#trip-info').dialog('close');
}


var trip_stages = '';
function show_trip_info(stagecode, tripcode){
    ga('send', 'pageview', '/trip/more'); //  track virtual page view
    $('#tabs_a_exp a:first').tab('show');
    
    $('#g-map').html('');
    $('#i-boarding').html('');
    
    $('#trip-info').removeClass('hidden');
    $('#trip-info').dialog({
        autoOpen: true,
        height: 600,
        width: 950,
        modal: true,
        resizable: false,
        clickOutside: true
    });
    $(".ui-dialog-titlebar").hide();
    
    $('a[href="#t-map"]').on('shown.bs.tab', function (e) {        
        init_route_map(trip_stages);
    })
    
    $('#i-from').html( $('.from').html() );
    $('#i-to').html( $('.to').html() );
    $('#i-date').html( $('.date').html() );
    
    //  staion points
    var data = {};
    data.tripCode = tripcode;
    $.ajax({
         type: "POST",
         url: base_url + "search/stage-details",
         data: data,
         dataType: 'json',
         success: function(res) {
             trip_stages = res;
             if(res.status==1) {
                //  render station points
               var html = '<table class="table"><tr><th>Name</th><th>Address</th><th>Landmark</th><th>Time</th></tr>';
               $.each(res.data, function(key, stage) {
                    html += '<tr class="info bold"><td  colspan="4">'+stage.fromStation.name+' - '+dateToDateTime(stage.fromStation.dateTime,2)+'</td></tr>';
                    $.each(stage.fromStation.stationPoint, function(pbkey, bp) {
                        html += '<tr> <td>'+bp.name+'</td><td>'+bp.address+'<br>'+bp.number+'</td><td>'+bp.landmark+'</td><td>'+dateToDateTime(bp.dateTime,2)+'</td></tr>';
                    });                          
               });
               html += '</table>';
               $('#i-boarding').html(html);
               
               //init_route_map(res); // google map init function

               //$('#tabmap').bind( 'click', function() {
                   //init_route_map(res);
               //});
             } else {
                 
             }
         }
     });
     
    //  cancellation policy
    $('#policy-loader').removeClass('hidden');
    $('#policy-loader').html('Please wait...');
    var data = {};
    data.stageCode = stagecode;
    $.ajax({
         type: "POST",
         url: base_url + "search/cancel-terms",
         data: data,
         dataType: 'json',
         success: function(res) {             
             if(res.status==1) {
                $('#policy-loader').addClass('hidden');
                var policy = '';
                $.each(res.data.policyList, function(pbkey, bp) {
                    policy += '<tr> <td>'+bp.term+'</td><td>'+bp.deductionAmountTxt+'</td><td>'+bp.refundAmountTxt+'</td><td>'+bp.chargesTxt+'</td></tr>';
                });
                $('#i-policy').removeClass('hidden');
                $('#i-policy').find('.table>tbody').html(policy);
             } else{
                 $('#policy-loader').removeClass('hidden');
                 $('#policy-loader').html('Sorry. Problem in showing cancellation policy.');
             }
         }
     });
     
    

}


var seatStatusDetails = new Array();
var tripDate = new Array();
var fromStation = new Array();
var toStation = new Array();

var g_ticket = [];
var g_seats = {};

function reselect_bus() {
     $('#busmap-1').dialog('close');
     g_seats = {};
}
function goto_pass_details() {
    $('#busmap-1').find('#busmapalert').html('');
    
    if(g_seats.seats.length<=0 || !g_seats.seats) {
        $('#busmap-1').find('#busmapalert').html('Please select atleast one seat');
        return;
    } else if($('#busmap-1').find('#busmapBPid').val()=="") { 
        $('#busmap-1').find('#busmapalert').html('Please select boarding place');
        return;
    } else if($('#busmap-1').find('#busmapDPid').val()=="") { 
        $('#busmap-1').find('#busmapalert').html('Please select dropping place');
        return;
    }
    
    $('#busmap-1').find('#busmapalert').html('Please wait...');
    $('#busmap-1').find('#busmap-buttons').hide();
    
    g_seats.boading_code = $('#busmap-1').find('#busmapBPid').val();
    g_seats.boading_place = $('#busmap-1').find('#busmapBPid option:selected').text();
    g_seats.droping_code = $('#busmap-1').find('#busmapDPid').val();
    
    g_seats.trip_code = g_trip_code;
    g_seats.stage_code = g_stage_code;
    if(search_type==1 && return_date!="") { //  search return ticket
        var s_url = base_url+"search?fromCityCode="+from_code+"&toCityCode="+to_code+"&tripDate="+search_date+"&returnDate="+return_date+"&type=2";
        $('#frm_searcher').attr('action', s_url);
        $('#owticket').val( JSON.stringify(g_seats) );
        document.frm_searcher.submit();
    } else if(search_type==1 && return_date=="") { // return date not choosed
        g_ticket.push(g_seats);
        $('#data').val( JSON.stringify(g_ticket) );
        document.frm_seats.submit();
    } else if(search_type==2) { // return trip data choosed
        g_ticket.push(owticket);
        g_ticket.push(g_seats);
        $('#data').val( JSON.stringify(g_ticket) );
        document.frm_seats.submit();
    }
}
function skip_return_trip() {
    g_ticket.push(owticket);
    $('#data').val( JSON.stringify(g_ticket) );
    document.frm_seats.submit();
}



function get_ticket_info() {
    $('#pasDetails').html('');
    $('#return-ticket').html('');
    $('[data-toggle="popover"]').popover('hide');
    var data = {};
    data.pnr = $.trim($('#pnr').val());    
    if(data.pnr=="") {
        $('#error').html('<div class="alert alert-danger">Please enter PNR number</div>');
        $('#pnr').focus();
        return;
    }

    $('#error').html('<div class="alert alert-danger">Please wait...</div>');
    $.ajax({
         type: "POST",
         url: base_url + "search/ticket-info",
         data: data,
         dataType: 'html',
         success: function(res) {
             $('#error').html('');
             $('#pasDetails').html(res);
                          
             /*$('#send-sms').popover({
                container:'body',
                content:function() {
                    var pnr = $(this).attr('data-pnr');
                    var html = '<div class="send-sms-form"> <div><span class="bold" width="50px">PNR</span> : '+pnr+'</div>'+                                
                               '<div><span class="bold" width="50px">Mobile</span> : <input id="sms-mobile"/></div>'+
                               '<div id="send-sms-action" class="text-right text-danger"></div>'+
                               '<div class="text-right"> <a class="action-button" href="javascript:;" onclick=send_sms("'+pnr+'") > Send </a> </div> </div>';
                    return html;
                },
                html:true,
                placement:'top',
                trigger:'click',
             });*/
         }
     });
}

function send_sms(pnr){ 
    var data = {};
    if(!pnr)
        data.pnr = $.trim($('#pnr').val());
    else
        data.pnr = pnr;
    /*data.mobile = $.trim($('#sms-mobile').val()); 
    if(data.mobile=="") {
        $('#send-sms-action').html('Please enter mobile');
        $('#sms-mobile').focus();
        return;
    } else if(!is_phone(data.mobile)) {
         $('#send-sms-action').html('Please enter valid mobile');
         $('#sms-mobile').focus();
        return;
    }*/
    
    $('#error').html('<div class="alert alert-danger">Please wait...</div>');
    $.ajax({
         type: "POST",
         url: base_url + "search/ticket-send-sms",
         data: data,
         dataType: 'json',
         success: function(res) {
             if(res.status==1) {
                 $('#error').html('<div class="alert alert-success">SMS has been sent successfully</div>');
                 window.setTimeout(function() {
                    $('#error').html('');
                 }, 3000);
             } else {
                 //$('#send-sms-action').html(res.errorDesc);
                 $('#error').html('<div class="alert alert-danger">'+res.errorDesc+'</div>');
             }
         }
     });
}

function get_return_ticket_info(code) {
    var data = {};
    data.pnr = code;
    data.return_ticket = 1;
    $('#return-ticket').html('<div class="alert alert-danger">Please wait...Loading return ticket</div>');
    $.ajax({
         type: "POST",
         url: base_url + "search/ticket-info",
         data: data,
         dataType: 'html',
         success: function(res) {
             $('#return-ticket').html(res);
         }
     });
}
function toggle_all_info() {
    var state = $('#autofil').is(':checked');
    
    if(state) {
        $('.pass-info').each( function() {
            $(this).find('#nameId').removeClass('hidden');
            $(this).find('#ageId').removeClass('hidden');
        } );
    } else {
        $('.pass-info').each( function() {
            $(this).find('#nameId').addClass('hidden');
            $(this).find('#ageId').addClass('hidden');
        } );
        $('.onward-trip').find('.pass-info').find('#nameId').first().removeClass('hidden');
        $('.onward-trip').find('.pass-info').find('#ageId').first().removeClass('hidden');
        
        $('.return-trip').find('.pass-info').find('#nameId').first().removeClass('hidden');
        $('.return-trip').find('.pass-info').find('#ageId').first().removeClass('hidden');
    }
}

function apply_offer() {
    $('#offer-status').html('');
    var offer_code = $.trim( $('#offercode').val() );
    if(offer_code == "") {
        $('#offer-status').html('Please enter offer code');
        $('#offercode').focus();
        return false;
    }
    var all_data = init_block_seats(1);
    if(all_data) {
        $('#offer-status').html('Please wait...');
        all_data.offer_code = offer_code;
        $.ajax({
             type: "POST",
             url: base_url + "search/validate-offer",
             data: all_data,
             dataType: 'json',
             success: function(res) {                  
                 if(res.status==1) {
                     var offer_amount = 0;
                     var value = res.data.value;
                     var maxvalue = res.data.maxValue | 0;                     
                     if(res.data.percentageFlag) {  //  % offer
                         offer_amount = (value/100) * total_seat_fare;
                     } else {   // flat offer
                         offer_amount = value;
                     }
                     if(Number(maxvalue) > 0 && Number(offer_amount) > Number(maxvalue)) {
                         offer_amount = maxvalue;
                     }
                     offer_amount = offer_amount.toFixed(2);                     
                     var total_fare = final_total - offer_amount;
                     
                     $('#offer-discount-label').removeClass('hide');
                     $('#offer-discount-label').find('.offervalue').html( '-'+offer_amount );
                     $('.final-total').html(total_fare);
                     $('.final-total-2').html(total_fare);
                     
                     $('#offer-status').html('<span class="text-success">A discount of '+offer_amount+' has been applied</span>');
                     
                 } else {
                    $('#offer-discount-label').addClass('hide');
                    $('.final-total').html(final_total);
                    $('.final-total-2').html(final_total);
                     
                    $('#offer-status').html(res.errorDesc);
                 }
             }
         });
     }
}

function init_block_seats(is_offer_validation){
   $('#ErrorMsgId').html(''); 
   var need_all_info = $('#autofil').is(':checked');
   
    if(!need_all_info) {
        var first_name = $('.onward-trip').find('.pass-info').find('#nameId').first().val();
        var first_age  = $('.onward-trip').find('.pass-info').find('#ageId').first().val();     
        $('.onward-trip').find('.pass-info').each( function() {
             $(this).find('#nameId').val(first_name);
             $(this).find('#ageId').val(first_age);
        } );
        
       if( $('.return-trip') ) {
            var first_name = $('.return-trip').find('.pass-info').find('#nameId').first().val();
            var first_age  = $('.return-trip').find('.pass-info').find('#ageId').first().val();     
            $('.return-trip').find('.pass-info').each( function() {
                 $(this).find('#nameId').val(first_name);
                 $(this).find('#ageId').val(first_age);
            } ); 
       } 
    }
  
   var contacts = {};
   contacts.mobile = $.trim( $('#phoneId').val() );
   contacts.email = $.trim( $('#emailId').val() );  
   
   //var all_data = {};
   var tickets = [];
   var data = {};
   
   if(contacts.mobile=="") {
       $('#ErrorMsgId').html('Please enter mobile number');
       $('#phoneId').focus();
       return false;
   }
   if(!is_phone(contacts.mobile)) {
       $('#ErrorMsgId').html('Please enter valid mobile number');
       $('#phoneId').focus();
       return false;
   }
   
   if(contacts.email=="") {
       $('#ErrorMsgId').html('Please enter email address');
       $('#emailId').focus();
       return false;
   }
   if(!is_email(contacts.email)) {
       $('#ErrorMsgId').html('Please enter valid email address');
       $('#emailId').focus();
       return false;
   }
   seat_infos = [];   
   var error = false;
   $('.onward-trip').find('.pass-info').each( function() {
        var seat_info = {};
        if($(this).find('#nameId').val()=="") {
          $('#ErrorMsgId').html('Please enter name');
          $(this).find('#nameId').focus();
          error = true;
          return false;  
        }

        if( $(this).find('#ageId').val()=="") {
           $('#ErrorMsgId').html('Please enter age');
           $(this).find('#ageId').focus();
           error = true;
           return false;  
        }
        
        if( !$.isNumeric($(this).find('#ageId').val()) ) {
           $('#ErrorMsgId').html('Please enter valid age');
           $(this).find('#ageId').focus();
           error = true;
           return false;  
        }
        
        var gen = $(this).find('input:radio[class=gender]:checked').val();
        if( gen !="M" && gen !="F") {
           $('#ErrorMsgId').html('Please select gender');
           $(this).find('input:radio[class=gender]').first().focus();
           error = true;
           return false;   
        }
        seat_info.code = $(this).find('#seat_code').val();
        seat_info.pname = $(this).find('#nameId').val();
        seat_info.age = $(this).find('#ageId').val();
        seat_info.fare = $(this).find('#seat_fare').val();
        seat_info.sname = $(this).find('#seat_name').val();
        seat_info.gender = gen;
        
        seat_infos.push(seat_info);
   } );
      
   tickets[0] = seats[0];
   tickets[0].seat_infos = seat_infos;
   
      
   seat_infos = []; 
   if( $('.return-trip').length >0 && !error ) {
        $('.return-trip').find('.pass-info').each( function() {
             var seat_info = {};
             if($(this).find('#nameId').val()=="") {
               $('#ErrorMsgId').html('Please enter name');
               $(this).find('#nameId').focus();
               error = true;
               return false;  
             }

             if( $(this).find('#ageId').val()=="") {
                $('#ErrorMsgId').html('Please enter age');
                $(this).find('#ageId').focus();
                error = true;
                return false;  
             }

             if( !$.isNumeric($(this).find('#ageId').val()) ) {
                $('#ErrorMsgId').html('Please enter valid age');
                $(this).find('#ageId').focus();
                error = true;
                return false;  
             }

             var gen = $(this).find('input:radio[class=gender]:checked').val();
             if( gen !="M" && gen !="F") {
                $('#ErrorMsgId').html('Please select gender');
                $(this).find('input:radio[class=gender]').first().focus();
                error = true;
                return false;   
             }
             seat_info.code = $(this).find('#seat_code').val();
             seat_info.pname = $(this).find('#nameId').val();
             seat_info.age = $(this).find('#ageId').val();
             seat_info.fare = $(this).find('#seat_fare').val();
             seat_info.sname = $(this).find('#seat_name').val();
             seat_info.gender = gen;

             seat_infos.push(seat_info);
        } );

        tickets[1] = seats[1];
        tickets[1].seat_infos = seat_infos;   
   }
   //data.payment.pay_gateway = $('#payment-options').val();
   /*if( data.pay_gateway == "") {
        $('#action_status').html('Please select payment gateway');
        $('#payment-options').focus();
        error = true;
        return false;  
   }*/
    
   if(!error && is_offer_validation==1) {   // called for offer validation
        var all_data = {};
        all_data.data = {};
        all_data.data.contacts = contacts;
        all_data.data.payment = $('#payment-options').val();
        all_data.data.tickets = tickets;
        return all_data;
   } 
   
   if(!error) {  
    if( $('#terms').is(':checked')==false  ) {
        $('#ErrorMsgId').html('Please accept the cancellation policy and terms & conditions');
         error = true;
        return false;
    } 
   } 
   if(!error) {       
    //seats.pass_info = data;
    var all_data = {};
    all_data.data = {};
    all_data.data.contacts = contacts;
    all_data.data.payment = $('#payment-options').val();
    all_data.data.tickets = tickets;
    
    all_data.data.offercode = $.trim( $('#offercode').val() );
    
    $('#ErrorMsgId').html('Please wait...');
    $('.pay-but').hide();
    $.ajax({
         type: "POST",
         url: base_url + "search/block-seats",
         data: all_data,
         dataType: 'json',
         success: function(res) {
             if(res.status==1) {
                if(res.data.paymentGatewayProcessFlag) {
                    var formControl = '';
                    $.each(res.data.gatewayInputDetails, function(rekey, reval) {                    
                        formControl += '<input name="' + rekey + '" id="' + rekey + '" value="' + reval + '" />';
                    });
                    //$('#frm_payment').html(formControl);
                    //$('#frm_payment').attr('action', res.paymentRequestUrl);                
                    $("#payment-form").html('<form action="' + res.data.paymentRequestUrl + '" name="frm_payment" id="frm_payment" method="post" style="display:none;">' + formControl + '</form>');
                    $("#frm_payment").submit();
                } else {
                    var url = base_url+'search/payment-response?orderCode='+res.data.code;
                    window.location.href = url;
                }
             } else {
                 $('.pay-but').show();
                 $('#ErrorMsgId').html(res.errorDesc);
             }
         }
     });
  }
    
}

function close_seatmap() {
  $('#busmap-1').dialog('close'); 
}
g_trip_code = '';
g_stage_code = '';
function init_busmap(code, tripcode) {
    g_seats.from_code = from_code;
    g_seats.to_code = to_code;
    g_seats.from_station = from_station;
    g_seats.to_station = to_station;
    g_seats.search_date = search_date;
    g_seats.return_date = return_date;
    
    // global purpose
    g_trip_code = tripcode;
    g_stage_code = code;
    
    g_seats.seats = [];
    $('#busmap-1').dialog({
        autoOpen: true,
        height: 550,
        width: 900,
        modal: true,
        resizable: false
    });
    $(".ui-dialog-titlebar").hide();
    $('#busmap-1').html('<br/><br/><div class="text-center">'+loading_big+'</div>');
    g_seats.stage_code = code;
    var data = {};
    data.code = code;
    var id = 1;
    $.ajax({
        type: "POST",
        url: base_url + "search/busmap",
        data: data,
        dataType: 'json',
        success: function(res) {
            if(res.status==1) {
                seatStatusDetails[id] = res.busmap.bus.seatLayoutList;
                render_bus_map(res, id);
                $("[data-toggle='tooltip']").tooltip();
                $('[data-toggle=popover]').popover({
                    container: "body",
                    html: true
                });
            } else {
              $('#busmap-1').html("<br><br><div class='alert alert-info text-center'>Sorry! Problem in loading bus details. Please try again later.</div>");  
            }
            
        }
    });
}

function render_bus_map(bus, id){
    var html = '';
    var upst;
    var lwst;
    var busmap = $('#template-busmap-list').html();
    busmap = $('<div>').html(busmap).clone();
    
    var result = render_bus_seats(bus.lrowmax, bus.lcolmax, bus.lrowmin, bus.lcolmin, bus.seatStatus, bus.seatDetails, 1, 1);
    
    html = result[0];

    if(result[1]!=''){
        lwst = result[1];
    }

    busmap.find('.lower').html(html);
    busmap.find('.searchhead_route').html("&nbsp;" + bus.busmap.fromStation.name+' - '+bus.busmap.toStation.name);
    //busmap.find('.to-sta').html("&nbsp;" + bus.busmap.toStation.name);
    busmap.find('.searchhead_date').html("&nbsp;" + dateToDateTime(bus.busmap.fromStation.dateTime, 1));
    busmap.find('.searchhead_time').html("&nbsp;" + dateToDateTime(bus.busmap.fromStation.dateTime, 2));
    
    tripDate[id] = bus.busmap.fromStation.dateTime;
    fromStation[id] = bus.busmap.fromStation.code;
    toStation[id] = bus.busmap.toStation.code;
    if (bus.urowmax != '' && bus.urowmax != undefined && bus.ucolmax != '' && bus.ucolmax != undefined) {
        var result = render_bus_seats(bus.urowmax, bus.ucolmax, bus.urowmin, bus.ucolmin, bus.seatStatus, bus.seatDetails, 2, id);
        html = result[0];
        if(result[1]!=''){
            upst = result[1];
        }
        busmap.find('.upper').html(html);
        busmap.find('.upper-layer').removeClass('hide');
        busmap.find('.seat-map-selayer').removeClass('hide');
    } else {    // center the map.
        //busmap.find('.lower-layer').removeClass('col-lg-offset-1').addClass('col-lg-offset-2');
        //busmap.find('#seat-legend2').removeClass('hide');
        //busmap.find('#seat-legend1').addClass('hide');
    }


    $("#busmap-" + id).html(busmap.html());
    if(lwst!=''){
        $("#busmap-" + id).find(lwst).show();
    }
    if(upst!=''){
        $("#busmap-" + id).find(upst).show();
    }
    
    //  set boarding point
    var option = '<option value="">- Bording Points- </option>';
    $.each(bus.busmap.fromStation.stationPoint, function(ke, svalue) {
        option += '<option value="' + svalue.code + '">' + svalue.name + ' - ' + dateToDateTime(svalue.dateTime,2) + '</option>';
    });
    $("#busmap-" + id).find('#busmapBPid').html(option);
    
    //  set droping point
    var option = '';
    var option = '<option value="">- Dropping Points - </option>';
    $.each(bus.busmap.toStation.stationPoint, function(dkey, dval) {
        option += '<option value="' + dval.code + '">' + dval.name + '</option>';
    });
    $("#busmap-" + id).find('#busmapDPid').html(option);
    
    //  bind seat actions
    $("#busmap-" + id).find('.seat-select-visibility').click(function() {          
        var seat_code = $(this).attr('data-code'); 
        var seat_name = $(this).html();
        
        $(this).toggleClass("seat-selected");              
        
        var seats = [];
        var seat_names = [];
        var base_fare = 0;
        var service_tax = 0;
        var discount = 0;
        $("#busmap-" + id).find('.seat-selected').each(function() {
            var seat = {};
            seat.code = $(this).attr('data-code');
            seat.name = $(this).html();
            seat.fare = $(this).attr('data-fare');
            seat.discount_fare = $(this).attr('data-discount');
            seat.service_charge = $(this).attr('data-service');
            seat.type = $(this).attr('data-type');
            seat.seat_type = $(this).attr('data-seattype');
            
            seats.push(seat);
            seat_names.push( $(this).html() );
            base_fare = Number(base_fare) + Number($(this).attr('data-fare'));
            
            if(Number($(this).attr('data-service')) >0) 
                service_tax = Number(service_tax) + Number($(this).attr('data-service'));
            
            if(Number($(this).attr('data-discount')) >0) 
                discount = Number(discount) + Number($(this).attr('data-discount'));
        }); 
        var total = Number(base_fare);
        
        //  add servie tax
        if(Number(service_tax)>0) 
            total = Number(total) + Number(service_tax);
        
        //  minus discount
        if(Number(discount)>0) 
            total = Number(total) - Number(discount);
        
        g_seats.seats = seats;
        seats = '';
        $("#busmap-" + id).find('#selectedSeats').html("<span class='text-danger'>"+seat_names.join(', ')+"</span>");
        $("#busmap-" + id).find('#selectedfare').html("<span class='text-danger'> <i class='fa fa-rupee'></i> "+ Number(base_fare).toFixed(2) +"</span>");
        $("#busmap-" + id).find('#selecteddiscount').html("<span class='text-danger'> <i class='fa fa-rupee'></i> "+ Number(discount).toFixed(2) +"</span>");
        $("#busmap-" + id).find('#selectedstax').html("<span class='text-danger'> <i class='fa fa-rupee'></i> "+ Number(service_tax).toFixed(2) +"</span>");
        if(parseInt(service_tax) > 0 ) {
            $('#tr-stax, #tr-stotal').removeClass('hidden');            
        } else {
            $('#tr-stax, #tr-stotal').addClass('hidden');
        } 
        if(parseInt(discount) > 0 ) {
            $('#tr-discount').removeClass('hidden');            
        } else {
            $('#tr-discount').addClass('hidden');
        } 
        $("#busmap-" + id).find('#selectedtotal').html("<span class='text-danger'> <i class='fa fa-rupee'></i> "+Number(total).toFixed(2)+"</span>");
        
    });
}

function boarding_address(code) {
    $('#boarding-address').html('');
    $.each(trips_json, function(key, trip) {
        if( g_trip_code == trip.tripCode) {
            $.each(trip.fromStation.stationPoint, function(pbkey, bp) {
                if(bp.code==code) {
                    $('#boarding-address').html('<div class="well well-sm">'+bp.name+', '+bp.address+', '+bp.landmark+'</div>');
                }
            });
        }
    });
}

function dropping_address(code) {
    $('#dropping-address').html('');
    $.each(trips_json, function(key, trip) {
        if( g_trip_code == trip.tripCode) {
            $.each(trip.toStation.stationPoint, function(pbkey, bp) {
                if(bp.code==code) {
                    $('#dropping-address').html('<div class="well well-sm">'+bp.name+', '+bp.address+', '+bp.landmark+'</div>');
                }
            });
        }
    });
}

function render_bus_seats(row, col, rowmin, colmin, seatstatus, seatDetails, lay, id) {
    var s = '';
    var htm ='';
    var seatStatusList = {
        "AM": "Available male", 
        "AF": "Available female", 
        "AL": "Available", 
        "BO": "Booked", 
        "BM": "Booked for Male", 
        "BF": "Booked for Female", 
        "AY": "", 
        "AO": "Booked", 
        "UK": "", 
        "TBL": "Booked", 
        "PBL": "Booked", 
        "BL": "Booked"
    };
    var seatStatusClass = {
        "AM": "seat-available-gents", 
        "AF": "seat-available-ladies", 
        "AL": "", 
        "BO": "seat-booked", 
        "BM": "seat-booked-gents", 
        "BF": "seat-booked-ladies", 
        "AY": "seat-available-you", 
        "AO": "seat-booked seat-booked-gents", 
        "UK": "seat-booked", 
        "TBL": "seat-booked seat-booked-gents", 
        "PBL": "seat-booked seat-booked-gents", 
        "BL": "seat-booked seat-booked-gents"
    };
    var rowSeatFoundCount = {};
    for (var i = rowmin; i <= row; i++) {
        for (var j = colmin; j <= col; j++) {
            var set = lay + "" + i + "" + j;
            if(rowSeatFoundCount[i] != undefined){
                rowSeatFoundCount[i] = rowSeatFoundCount[i]+1;
            }else{
                rowSeatFoundCount[i] = 1;
            }
        }
    }
    var axialFound = false;
    var preSeatType = '';
    for (var i = rowmin; i <= row; i++) {
        var sleeperSeatFound = false;
        for (var j = colmin; j <= col; j++) {
            var set = lay + "" + i + "" + j;
            var seatclass = '';
            var BusSeatTypeclass = ' seat-lay-search ';
            
            var c = '';
            var sn = ''

            if (seatstatus[set] != undefined) {

                var dataPop = "<div style='text-align:center'>" + seatstatus[set]['seatType'] + " - <i class='fa fa-rupee'></i> " + seatstatus[set]['fare'] + "<br />" + seatStatusList[seatstatus[set]['status']['code']] + "";
                //  discount
                if( Number(seatstatus[set]['discountFare']) > 0 ) {
                    dataPop += " - <span class='text-info'><i class='fa fa-rupee'></i> " + seatstatus[set]['discountFare'] + ' off </span>';
                }
                
                if (seatstatus[set]['code'] != undefined && seatstatus[set]['status'] != undefined) {
                    if(htm!='') htm += ',';

                    if(htm.indexOf(seatstatus[set]['code']) == -1) htm = seatstatus[set]['status']['code']
                    if (seatstatus[set]['status']['code'] != 'BO' &&  seatstatus[set]['status']['code'] != 'BL' 
                            && seatstatus[set]['status']['code'] != 'AO' && seatstatus[set]['status']['code'] != 'TBL' 
                            && seatstatus[set]['status']['code'] != 'BF' && seatstatus[set]['status']['code'] != 'BM' 
                            && seatstatus[set]['status']['code'] != 'PBL' && seatstatus[set]['status']['code'] != undefined) {
                        seatclass += ' seat-select-visibility ' + seatStatusClass[seatstatus[set]['status']['code']];
                    } else {
                        seatclass += seatStatusClass[seatstatus[set]['status']['code']];
                    }
                    var scode = seatstatus[set]['code'];
                    c = ' data-code = "' + seatstatus[set]['code'] + '" data-fare = "'+ seatstatus[set]['fare'] +'" data-discount = "'+ seatstatus[set]['discountFare'] +'" data-service = "'+ seatDetails[scode]['serviceTax'] +'" data-seattype = "'+seatstatus[set]['status']['code']+'" ';
                    /*if (seatstatus[set]['status']['code'] == 'AO' || seatstatus[set]['status']['code'] == 'BL') {
                        var temp = '';
                        if (seatstatus[set]['userName'] != undefined) {
                            temp += '&nbsp;' + seatstatus[set]['userName']+"(u)"; //userName
                        }
                        if (seatstatus[set]['groupName'] != undefined) {
                            if(temp!='') temp += ',';
                            temp += '&nbsp;' + seatstatus[set]['groupName']+"(g)";
                        }
                        if(temp!=''){
                            dataPop += temp;
                        }
                    } */
                    dataPop += "</div>";
                    sn = seatDetails[seatstatus[set]['code']]['seatName'];
                    if (seatstatus[set]['status']['code'] == 'BO' || seatstatus[set]['status']['code'] == 'PBL') {                        
                        $.each(seatStatusDetails[id], function(t, tik) {
                            if (seatstatus[set]['code'] == tik.code && tik.ticketCode != '' && tik.ticketCode != undefined) {                                
                                if (tik.seatGendarStatus != undefined) {
                                    if (tik.seatGendarStatus.code == "M" && tik.seatGendarStatus.code != undefined) {
                                        seatclass += ' seat-booked-gents ';
                                    } else if (tik.seatGendarStatus.code == "F" && tik.seatGendarStatus.code != undefined) {
                                        seatclass += ' seat-booked-ladies ';
                                    }
                                }
                            }
                        });                        
                    }
                }
                
                if (seatstatus[set]['status'] != '' && seatstatus[set]['status'] != undefined && seatstatus[set]['status'] == 1 && seatstatus[set] != undefined) {
                    seatclass += ' seat-booked ' + seatStatusClass[seatstatus[set]['status']['code']];
                }
                if (seatstatus[set]['seatType'] != '' && seatstatus[set]['seatType'] != undefined && (seatstatus[set]['seatType'] == "Sleeper" || seatstatus[set]['seatType'] == "Upper Sleeper" || seatstatus[set]['seatType'] == "Lower Sleeper") )  {
                    BusSeatTypeclass =' sleeper-lay-search ';
                    sleeperSeatFound = true;
                }else{
                    sleeperSeatFound = false;
                }
                preSeatType = seatstatus[set]['seatType'];
            } else if(rowSeatFoundCount[i] != undefined && rowSeatFoundCount[i] > 0 && !sleeperSeatFound){// && seatstatus[previousSeatPos]['seatType'] != '' && seatstatus[previousSeatPos]['seatType'] != undefined && seatstatus[previousSeatPos]['seatType'] == "Sleeper" ){
                sn = '&nbsp;';
                dataPop = '';
                if(preSeatType == "Sleeper" || preSeatType == "Upper Sleeper" || preSeatType == "Lower Sleeper") BusSeatTypeclass =' sleeper-lay-search ';
            }else if(rowSeatFoundCount[i] == 0 && !axialFound){
                sn = '&nbsp;';
                axialFound = true;
                dataPop = '';
            }
            
            if(sn != ''){
                s += '<div class="p_tool  seat-lay-out ' + BusSeatTypeclass +seatclass +  '" ' + c 
                if(dataPop !='' )
                    s += ' data-toggle="popover" data-container="body" data-placement="left" data-trigger="hover" data-content="' + dataPop + '"'
                s +=' >' + sn + '</div>';
            }
        }
        s += '<div class="clear_fix_both clearfix"></div>';
    }
    return [s,htm];
}

function get_ticket_info_for_cancel() {
    $('#pasDetails').html('');
    $('#return-ticket').html('');
    var data = {};
    data.pnr = $.trim($('#pnr').val());
    data.email = $.trim($('#email-adr').val());
    if(data.pnr=="") {
        $('#error').html('Please enter PNR number');
        $('#pnr').focus();
        return;
    }
    if(data.email=="") {
        $('#error').html('Please enter email address');
        $('#email-adr').focus();
        return;
    } else if(!is_email(data.email)) {
        $('#error').html('Please enter valid email address');
        $('#email-adr').focus();
        return;
    }    
    
    $('#error').html('Please wait...');
    $.ajax({
         type: "POST",
         url: base_url + "search/ticket-info-for-cancel",
         data: data,
         dataType: 'html',
         success: function(res) {
             $('.cancel-form').hide();
             $('#error').html('');
             $('#pasDetails').html(res);
         }
     });
}

function calculate_refund(){
    var seatfare = 0;
    var tax = 0;
    var refund  = 0;
    var deduction  = 0;
    var checked = false;
    $('.chk_cancel').each( function() {
        if( $(this).is(':checked') ){
            seatfare = Number(seatfare) + Number($(this).attr('data-seatfare'));
            tax = Number(tax) + Number($(this).attr('data-service'));
            refund = Number(refund) + Number($(this).attr('data-refund'));
            deduction = Number(deduction) + Number($(this).attr('data-deduction'));
            checked = true;
        }        
    });
    if(checked==true) {
        var tmp = $('#refund_info_template').html();
        template = $(tmp).clone();

        $(template).find('#seatfare').html(Number(seatfare).toFixed(2));
        $(template).find('#tax').html(Number(tax).toFixed(2));
        $(template).find('#refund').html(Number(refund).toFixed(2));
        $(template).find('#deduction').html(Number(deduction).toFixed(2));

        $('#cancel_info_details').html(template);        
        $('#cancel_info').removeClass('hidden');
        $('#cancel-btn').addClass('hidden');
    }
}

function close_refund_info() {
    $('#cancel_info').addClass('hidden');
    $('#cancel-btn').removeClass('hidden');
    $('.chk_cancel').each( function() {
        $(this).removeAttr('checked');       
    });
}

function confirm_cancel() {
    var seats = [];
    $('.chk_cancel').each( function() {
        if( $(this).is(':checked') ){
            seats.push($(this).val());
        }        
    });
    
    var data = {};
    data.pnr = $.trim($('#pnr').val());
    data.email = $.trim($('#email-adr').val());
    data.seatcode = seats;
    
    $('#cancel_error').html('Please wait...');
    $('#btn-panel').hide();
    $.ajax({
        type: "POST",
        url: base_url + "search/ticket-cancel-confirm",
        data: data,
        dataType: 'json',
        success: function(res) {
            if(res.status==1) {
                $('#cancel_error').html('Your request has been processed successfully.'); 
                window.setTimeout(function() {
                    window.location.href = base_url;
                 }, 2000);
            } else {
                $('#cancel_error').html(res.errorDesc);
            }
        }
    });
}

function init_route_map(stages) {
    ga('send', 'pageview', '/trip/more/route-map'); //  track virtual page view
    //   init map tab
    var source = [];
    var start = [];
    var c = parseInt(1);
    var loc = [];
    $.each(stages.data, function(key, stage) {           
        loc.push('<span class="f14"> <i class="fa fa-map-marker"></i> '+stage.fromStation.name+' </span> &nbsp; ');
        loc.push('<i class="fa fa-long-arrow-right f14"></i> &nbsp; ');
        $.each(stage.fromStation.stationPoint, function(pbkey, bp) {
            var data = {};
            data.name = bp.name;
            data.latitude = bp.latitude;
            data.longitude = bp.longitude;
            data.address = bp.address;
            data.landmark = bp.landmark;
            data.time = dateToDateTime(bp.dateTime,2);
            if(c==1) start.push(data);
            else source.push(data);
            c++;
        });                          
    });
    loc.pop();
    var t = loc.join(' ');
    $('.map-locations').html(loc);
            
    var opt = {};
    opt.element = 'g-map';
    opt.startpoint = start[0]; 
    opt.endpoint = source.pop();
    opt.source = source;    
    opt.center = {lat:'13.06917', lang:'80.19139'};    
    
    myRouter.init(opt);
    
}

function show_cancel_policy() {
    $('#pop-policy').removeClass('hidden');
    $('#pop-policy').dialog({
        autoOpen: true,
        height: 600,
        width: 950,
        modal: true,
        resizable: false,
        clickOutside: true
    });
    $(".ui-dialog-titlebar").hide();
    
    //  cancellation policy
    $('#policy-loader').removeClass('hidden');
    $('#policy-loader').html('Please wait...');
    var data = {};
    var stage_codes = [];
    if( seats[0].stage_code !="" ) stage_codes.push(seats[0].stage_code);
    if( seats[1] && seats[1].stage_code !="" ) stage_codes.push(seats[1].stage_code);
    
    data.stageCode = stage_codes;
    $.ajax({
         type: "POST",
         url: base_url + "search/cancel-policy",
         data: data,
         dataType: 'json',
         success: function(res) {      
             $('#policy-loader').addClass('hidden');
             
             if(res[0].status==1) { 
                $('.onward-trip-policy').removeClass('hidden'); 
                var policy = '';
                $.each(res[0].data.policyList, function(pbkey, bp) {
                    policy += '<tr> <td>'+bp.term+'</td><td>'+bp.deductionAmountTxt+'</td><td>'+bp.refundAmountTxt+'</td><td>'+bp.chargesTxt+'</td></tr>';
                });
                $('.onward-trip-policy').find('#i-policy').removeClass('hidden');
                $('.onward-trip-policy').find('#i-policy').find('.table>tbody').html(policy);
             }
             
             if(res[1] && res[1].status==1) {
                $('.return-trip-policy').removeClass('hidden');
                var policy = '';
                $.each(res[1].data.policyList, function(pbkey, bp) {
                    policy += '<tr> <td>'+bp.term+'</td><td>'+bp.deductionAmountTxt+'</td><td>'+bp.refundAmountTxt+'</td><td>'+bp.chargesTxt+'</td></tr>';
                });
                $('.return-trip-policy').find('#i-policy').removeClass('hidden');
                $('.return-trip-policy').find('#i-policy').find('.table>tbody').html(policy);
             }
         }
     });
}

function print_ticket(code) {
    var url = base_url + "search/print-ticket?pnrCode=" + code;    
    var wo = window.open(url, "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=50, left=50, width=1000, height=650");
}
var edit_new_points = '';
function update_boarding_point(){
    var data = {};
    data.pnr = $.trim($('#pnr').val());
    data.email = $.trim($('#email-adr').val());
    if(data.pnr=="") {
        $('#error').html('Please enter PNR number');
        $('#pnr').focus();
        return;
    }
    if(data.email=="") {
        $('#error').html('Please enter email address');
        $('#email-adr').focus();
        return;
    } else if(!is_email(data.email)) {
        $('#error').html('Please enter valid email address');
        $('#email-adr').focus();
        return;
    }
    $('#error').html('Please wait...');
    $.ajax({
         type: "POST",
         url: base_url + "search/update-boading-point",
         data: data,
         dataType: 'json',
         success: function(res) {
             if(res.status==1) {
                 $('#edit-form').addClass('hidden');
                 $('#boarding-container').removeClass('hidden');
                 var txt = res.oldpoint.name +', <br>'+res.oldpoint.address+', <br>'+res.oldpoint.landmark;
                 $('#old-point').html('<div class="well well-sm">'+txt+'</div>');
                 $('#old-boarding-code').val(res.oldpoint.code);
                 
                 var opt = '<option value=""> - Select - </option>';
                 $.each(res.newpoints, function(k, ele) {
                    opt += '<option value="'+ele.code+'">'+ele.name+' - '+ dateToDateTime(ele.dateTime,2) +'</option>'; 
                 });
                 $('#new-boarding').html(opt);
                 edit_new_points = res.newpoints;
             } else {
                 $('#error').html('<div class="alert alert-danger">'+res.errorDesc+'</div>');
             }             
         }
     });
}

function show_new_point_address(code) {
    $('#new-point').html('');
    $.each(edit_new_points, function(pbkey, bp) {
        if(bp.code==code) {
            $('#new-point').html('<div class="well well-sm">'+bp.name+', <br>'+bp.address+', <br>'+bp.landmark+'</div>');
        }
    });         
}

function update_mobile_number() {
    var data = {};
    data.pnr = $.trim($('#pnr').val());
    data.email = $.trim($('#email-adr').val());
    if(data.pnr=="") {
        $('#error').html('Please enter PNR number');
        $('#pnr').focus();
        return;
    }
    if(data.email=="") {
        $('#error').html('Please enter email address');
        $('#email-adr').focus();
        return;
    } else if(!is_email(data.email)) {
        $('#error').html('Please enter valid email address');
        $('#email-adr').focus();
        return;
    }

    $('#error').html('Please wait...');
    $.ajax({
         type: "POST",
         url: base_url + "search/update-mobile-number",
         data: data,
         dataType: 'json',
         success: function(res) {
             if(res.status==1) {
                 $('#edit-form').addClass('hidden');
                 $('#mobile-container').removeClass('hidden');
                 $('#old-mobile').html(res.mobile);
                 $('#new-mobile').focus();
             } else {
                 $('#error').html('<div class="alert alert-danger">'+res.errorDesc+'</div>');
             }
            //$('#error').html('');
            //$('#pasDetails').html(res);             
         }
     });
}

function save_mobile_number(){
    var data = {};
    data.pnr = $.trim($('#pnr').val());
    data.mobile = $.trim($('#new-mobile').val());
    data.oldmobile = $.trim($('#old-mobile').text());
    data.email = $.trim($('#email-adr').val());
    if(data.mobile=="") {
        $('#error').html('Please enter new mobile number');
        $('#new-mobile').focus();
        return;
    } else if(!is_phone(data.mobile)) {
        $('#error').html('Please enter valid new mobile number');
        $('#new-mobile').focus();
        return;
    }
    $('#mobile-error').html('Please wait...');
    $.ajax({
         type: "POST",
         url: base_url + "search/save-mobile-number",
         data: data,
         dataType: 'json',
         success: function(res) {
             if(res.status==1) {
                 $('#mobile-error').html('<div class="alert alert-success">New mobile number has been updated. <a href="'+base_url+'search/ticket?pnr='+data.pnr+'">View Ticket Details</a></div>');
             } else {
                 $('#mobile-error').html('<div class="alert alert-danger">'+res.errorDesc+'</div>');
             }
         }
     });
}

function save_boarding_point(){
    var data = {};
    data.pnr = $.trim($('#pnr').val());
    data.newboarding = $.trim($('#new-boarding').val());
    data.oldboarding = $.trim($('#old-boarding-code').val());
    data.email = $.trim($('#email-adr').val());
    if(data.newboarding=="") {
        $('#boarding-error').html('Please select a boarding point');
        $('#new-boarding').focus();
        return;
    }
    $('#boarding-error').html('Please wait...');
    $.ajax({
        type: "POST",
        url: base_url + "search/save-boarding-point",
        data: data,
        dataType: 'json',
        success: function(res) {
            if(res.status==1) {
                $('#boarding-error').html('<div class="alert alert-success">New boading point has been updated. <a href="'+base_url+'search/ticket?pnr='+data.pnr+'">View Ticket Details</a></div>');
            } else {
                $('#boarding-error').html('<div class="alert alert-danger">'+res.errorDesc+'</div>');
            }
        }
    });
}

function back_to_edit(){
    $('#error').html('');
    $('#boarding-error').html('');
    $('#mobile-error').html('');
    $('#edit-form').removeClass('hidden');
    $('#mobile-container').addClass('hidden');
    $('#boarding-container').addClass('hidden');
}

$(function() {    
    //  auto fill passenger details.
    $('#phoneId').blur(function() {
        var t = seats[0].seats;
        var data = {};
        data.phone = $.trim( $(this).val() );
        data.seats = t.length;
        $.ajax({
            type: "POST",
            url: base_url + "search/autocomplete-names",
            data: data,
            dataType: 'json',
            success: function(res) {
                if(res.status==1) {
                    if(res.data.passegerEmailId) 
                     $('#emailId').val(res.data.passegerEmailId); 
                    var l = res.data.ticketDetails.length;
                    var d = res.data.ticketDetails;
                    var ow = $('.onward-trip').find('.pass-info');
                    for(i=0; i<l; i++){
                        $(ow[i]).find('#nameId').val(d[i].passengerName);
                        $(ow[i]).find('#ageId').val(d[i].passengerAge);
                        $(ow[i]).find('.gender').each(function() {
                            if( $(this).val() == d[i].passengerGendar ) {
                                $(this).attr('checked','checked');
                            }
                        });
                    }
                    
                    var ow = $('.return-trip').find('.pass-info');
                    for(i=0; i<l; i++){
                        $(ow[i]).find('#nameId').val(d[i].passengerName);
                        $(ow[i]).find('#ageId').val(d[i].passengerAge);
                        $(ow[i]).find('.gender').each(function() {
                            if( $(this).val() == d[i].passengerGendar ) {
                                $(this).attr('checked','checked');
                            }
                        });
                    }
                   
                } 
            }
        });
    });
});

function dateToDateTime(time, type) {

    var dateString = time.replace(/-/g, "/");
    var dt = new Date(dateString);
    var hours = dt.getHours()
    var minutes = dt.getMinutes()
    var month = dt.getMonth() + 1;
    var day = dt.getDate();

    month = month + "";

    if (month.length == 1)
    {
        month = "0" + month;
    }

    day = day + "";

    if (day.length == 1)
    {
        day = "0" + day;
    }
    if (minutes < 10)
        minutes = "0" + minutes
    var suffix = "AM";
    if (hours >= 12) {
        suffix = "PM";
        hours = hours - 12;
    }
    if (hours == 0) {
        hours = 12;
    }
    if(type==1)
        return day + "/" + month + "/" + dt.getFullYear();
    else if(type==2)
        return hours + ":" + minutes + " " + suffix;
}



function isNull(a) {
    if (typeof a == "undefined") {
            return true;
    } else {
            if (a == null || trimez(a) == "") {
                    return true;
            } else {
                    return false;
            }
    }
}
function trimez(str){
	 return str.replace(/^\s+|\s+$/g, '');
}

function is_email(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}

function is_phone(str) {
    if(str.length<10) {
        return false;
    }
     var filter = /^[0-9\s]*$/i;
    if (filter.test(str)) {
        return true;
    } else {
        return false;
    }
}

function setCookie(c_name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : "; expires=" + exdate.toGMTString());
}
function doSearch(fromCity,toCity) {
    var data = {}
    data.fromcode = fromCity;
    data.tocode = toCity;
    data.tripDate = $('#sch_date').val();
    data.type = 1;
    var url = base_url+"search?fromCityCode="+ data.fromcode+"&toCityCode="+ data.tocode+"&tripDate="+data.tripDate+"&type="+data.type;
    $(location).attr("href", url);
}
//------------------------------------------------

