﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BusReservationSystem.Startup))]
namespace BusReservationSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
