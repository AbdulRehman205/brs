
$(function() {
	      $( "#source" ).autocomplete({
					source: function (req, response) {						
							var re = $.ui.autocomplete.escapeRegex(req.term);
							var matcher = new RegExp("^" + re, "i");
							response($.grep(stationslist, function (item) {
							if(matcher.test(item.label)){
							return matcher.test(item.label);
							}	
							}));
					},
					autoFocus: true,					
					minLength: 2,					
					select: function(event, ui) {
					$('#source_id').val(ui.item.id);
					}
	        })
				$( "#destination" ).autocomplete({
					source: function (req, response) {
							var re = $.ui.autocomplete.escapeRegex(req.term);
							var matcher = new RegExp("^" + re, "i");
							response($.grep(stationslist, function (item) {
							if(matcher.test(item.label)){
							return matcher.test(item.label);
							}	
							}));
					},
					autoFocus: true,
					minLength: 2,
					select: function(event, ui) {
					$('#destination_id').val(ui.item.id);
					}
	        })
		});		
function validateBusForm() { 

	var new_date = new Date();
	var current_month = new_date.getMonth();
	var current_day	  = new_date.getDate();
	var current_year  = new_date.getFullYear();

	//var tcount = document.frmFinal.trip.length
	/*for(var i=0;i<tcount;i++)
	{
		if(document.frmFinal.trip[i].checked==true)
		{
			document.frmFinal.Tripval.value = document.frmFinal.trip[i].value;
		}
	}*/

	if(document.frmFinal.journey_rdate.value!=''){
		document.frmFinal.Tripval.value ="R";
	}else{
		document.frmFinal.Tripval.value ="O";
	}

	if(document.frmFinal.source.value=="Enter a city or local area")
	{
		alert("Please select the Source city");
		document.frmFinal.source.focus();
        return false;
	}
	if(document.frmFinal.source_id.value=="")
	{
		alert("Please select the Source city");
		document.frmFinal.source.focus();
        return false;
	}
	if(document.frmFinal.destination.value=="Enter a city or local area")
	{
		alert("Please select the Destination");
		document.frmFinal.destination.focus();
        return false;
	}
	if(document.frmFinal.destination_id.value=="")
	{
		alert("Please select the Destination");
		document.frmFinal.destination.focus();
        return false;
	}
	var chkdt=document.frmFinal.journey_date.value;

	var chkrtrndt=document.frmFinal.journey_rdate.value;
	var chkFDate=document.frmFinal.FutureDate.value;
	var chkTrip=document.frmFinal.Tripval.value;

	// alert(chkFDate);  
	 sel_shop_day=chkdt.substring(0,2)
	//alert("sel_shop_day="+sel_shop_day)
	  sel_shop_month=chkdt.substring(3,5)
	//alert("sel_shop_month="+sel_shop_month)
	  sel_shop_year=chkdt.substring(6)
	 //alert("sel_shop_year="+sel_shop_year)

	  sel_ret_day=chkrtrndt.substring(0,2)
	//alert("sel_shop_day="+sel_shop_day)
	  sel_ret_month=chkrtrndt.substring(3,5)
	//alert("sel_shop_month="+sel_shop_month)
	  sel_ret_year=chkrtrndt.substring(6)
	//alert("sel_shop_year="+sel_shop_year)

	 if(sel_shop_year < current_year ) {
		 alert("Date of departure journey should not be less than today's date");
		 return false;
	 } else if(sel_shop_year == current_year) {
		 if(sel_shop_month < (current_month+1)) {
			alert("Date of departure journey should not be less than today's date");
			return false;
		 }
		 else if(sel_shop_month == (current_month+1)) {
				if(sel_shop_day < current_day) {
					alert("Date of departure journey should not be less than today's date");
					return false;
				}
		 }
	 }
	 //alert("TEST : "+chkTrip)
	if(chkTrip=="R"){
		if(sel_ret_year==''){		
		alert("Please Select Return Journey Date");
		return false;
     }	
		
	if(sel_ret_year < current_year ) {
		 alert("Date of return journey should not be less than today's date");
		 return false;
	 } else if(sel_ret_year == current_year) {
		 if(sel_ret_month < (current_month+1)) {
			alert("Date of return journey should not be less than today's date");
			return false;
		 }
		 else if(sel_ret_month == (current_month+1)) {
				if(sel_ret_day < current_day) {
					alert("Date of return journey should not be less than today's date");
					return false;
				}
		 }
	 }

	 if((Validate_DateDiff(document.frmFinal.journey_date.value,document.frmFinal.journey_rdate.value)==false))
		{
		 alert("Date of departure journey should not be greater than return journey date")
		 return false;
		}

	}

	 if((Validate_DateDiff(document.frmFinal.journey_date.value,document.frmFinal.FutureDate.value)==false))
		{
		 alert("Date of departure journey should not be greater than "+chkFDate+" date")
		 return false;
		}
	if(chkTrip=="R"){
		if((Validate_DateDiff(document.frmFinal.journey_rdate.value,document.frmFinal.FutureDate.value)==false))
		{
		 alert("Date of return journey should not be greater than "+chkFDate+" date")
		 return false;
		}
	}

	
	if(document.frmFinal.journey_rdate.value!=''){
           document.frmFinal.action = 'index-2.html';
	}else{
           document.frmFinal.action = 'index-2.html';
	}
    document.frmFinal.submit();
	performBusSearch();
 return true;
}

function performBusSearch() {
  
  var waitDisplayDiv = document.getElementById('searchWaitBusDisplayDiv');
  waitDisplayDiv.style.left = (document.body.clientWidth/2) - 200;
  waitDisplayDiv.style.display = '';
  document.body.scrollTop = 0;
  
  showBackground();
  
  document.frmFinal.action = 'register_details0f0e.html?tType=Main';
  document.frmFinal.submit();
}

function showBackground() {
    var waitDiv = document.getElementById('backgroundDiv');
    if (! document.body) {
      waitDiv.style.height = 100;
      waitDiv.style.width = 200;
    } else {
      waitDiv.style.height = document.body.clientHeight;
      waitDiv.style.width = document.body.clientWidth;
    }
    waitDiv.style.display = '';
    document.body.scrollTop = 0;
  }


function fnValidate()
{	
	if(document.frmlogin.uname.value=="")
	{
		alert("Enter User Name")
		document.frmlogin.uname.focus()
		return false
	}
	if(document.frmlogin.pword.value=="")
	{
		alert("Enter Password")
		document.frmlogin.pword.focus()
		return false
	}
}

function ValidTrip(trp){
		var elm = document.getElementById("rtn");
	var elm1 = document.getElementById("rdatepicker");
	if (trp.value == "O") {
		elm.style.display = "none";
		elm1.style.display = "none";
		document.frmFinal.journey_rdate.disabled = true;
		return false;
	} else if (trp.value == "R") {
		elm.style.display = "block";
		elm1.style.display = "block";
		document.frmFinal.journey_rdate.disabled = false;
		return false;
	}
	
}

function ValidTrip1()
{
		alert(hi);
		document.frmFinal.journey_rdate.disabled=true;
		document.getElementById("journey_rdate_trigger").style.display="none"
		return false;
}

function Validate_DateDiff(dateString1,dateString2) {
	var strSeparator = '-' 
		var arrayDate1 = dateString1.split(strSeparator);
		var arrayDate2 = dateString2.split(strSeparator);
		var sdate = new Date(arrayDate1[2],arrayDate1[1]-1,arrayDate1[0]);
		//var testdate = new Date(arrayDate[2],arrayDate[0]-1,arrayDate[1]);
		var edate = new Date(arrayDate2[2],arrayDate2[1]-1,arrayDate2[0]);
		if (sdate <= edate) 
			 return true;
		  else
			 return false;
}


var counterCnt = 1;
		function fillRoute(counter, source, destination){
			counterCnt = counter;
			getCityIn(source, destination);
		}	
		
		function getCityIn(source, destination){
			if(typeof stationslist == 'undefined'){
				$.ajax({
					 type: "GET",
					 url: "stationlist",
					 success:function(result){
						//console.log(result);
						if(result != null){
							stationslist = result;
							if(source != '' || destination != ''){
								//fillData(stationslist, source, destination);
							}
						}
					 }
				 });
			}else{
				fillData(stationslist, source, destination)
			}
		}
		function fillData(obj, source, destination){
			//console.log(obj)
			$.each(obj, function(i, item) {
				if(source == item.label){
					//document.frmFinal.source_id.focus();
					$( "#source" ).val('');
					$( "#source" ).val( item.label ); 
					document.frmFinal.source_id.value =  item.id;
				}
				if(destination == item.label){
					//document.frmFinal.destination_id.focus();
					$( "#destination" ).val('');
					$( "#destination" ).val( item.label ); 
					document.frmFinal.destination_id.value=  item.id;
					$("#datepicker").datepicker('show');
				}
			});
		}
