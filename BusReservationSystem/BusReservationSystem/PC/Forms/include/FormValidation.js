//******************************************************************
//
//Purpose	    : To Validate the special characters
//Parameters	: Control Value
//Return Value	: true/false
//Example for a : rtrim(Form1.TextBox1.Value)
//
//********************************************************************
function ValidateEmail(src) {
	var re = /^(([^<>()[\]\\.'/,;:\s@\"]+(\.[^<>()[\]\\.'/,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	if (src.match(re))
	{
		return true;
	}
	else
	{
		return false;
	}
}
 function noenter() 
 {
    return !(window.event && window.event.keyCode == 13);
 }
function Validate_PhoneNumber(ob) //This function is to check whether any speacial chars are there in phone fields or not.
   {
     str = ob;
    
     var poshiphen= str.indexOf("-");
     var pos=str.indexOf("+")
     var str;
     bag = "1234567890-+()";
     if(poshiphen==0)
     {
       //alert("Invalid Phone Number");
       return false;
     }
     if(pos>0)
     {
       //alert("Invalid Phone Number");
       return false;
     }
     if(pos==0 && poshiphen==1)
     {
       //alert("Invalid Phone Number");
       return false;
     }
     var Pluscntr;
	 Pluscntr=0;
	 for(cntr=0;cntr<str.length;cntr++)
		{
			
			if(str.charAt(cntr)=="+")
			{
				Pluscntr++;
			}
		}
	if(Pluscntr>1)
	{
	   //alert("Invalid Phone Number");
       return false;
	}
    if(str.indexOf("+")==0 && str.length==1)
    {
     // alert("Invalid Phone Number");
      return false;
    }
    if(chkHotlineChars(str)==false)
    {
      //alert("Invalid Phone Number");
      return false;
    }
    if(hotlinecheck(str)==false)
    {
     // alert("Invalid Telephone number.Telephone number cannot have continous -.")
      return false;
    }
      return true;
   }
   function hotlinecheck(lstrvalue)  //This function is not to allow continuos - variable.
   {
	var val = lstrvalue
	for (intRowCount=0; intRowCount < val.length-1 ; intRowCount++)
		 {
			chkChar = val.charAt(intRowCount)
			
			if(chkChar=="-")
			{ 
				
				
				if(val.charAt(intRowCount+1)=="-")
					{
					
					return false
					}
			}
			
		 }
   }
   function ForDotsinMail(ob)  //This function is to count the no.of dots in email strings.
   {
     k=0;
     for(i=0;i<ob.length-1;i++)
     {
       chkChar = ob.charAt(i)
       if(chkChar==".")
         k++;
       if(k>2)
       {
        alert("Email Id Doesn't Allow more than 2 dots.");
        return false;
       }
     }
   }
   function chkHotlineChars(strValue) //This function is to validate only some chars
	{
		
		var val = strValue
		
		var arrayAux
		arrayAux = new Array
		arrayAux[0] ="0"
		arrayAux[1] ="1"
		arrayAux[2] ="2"
		arrayAux[3] ="3"
		arrayAux[4] ="4"
		arrayAux[5] ="5"
		arrayAux[6] ="6"
		arrayAux[7] ="7"
		arrayAux[8] ="8"
		arrayAux[9] ="9"
		arrayAux[10] ="-"
		arrayAux[11] ="+"
		arrayAux[12] =" "
		arrayAux[13] ="("
		arrayAux[14] =")"
		
		 for (intRowCount=0; intRowCount < val.length ; intRowCount++)
		 {
			chkChar = val.charAt(intRowCount);
		
			for (ii=0; ii < arrayAux.length ; ii++)
			{
		
				if ( chkChar == arrayAux[ii])
					{
					
					flag=true;
					break;
					}
				else
					{
					
					flag=false;
					}
			}
		 if(flag==false)
		 return false;
		 }
		 //return true 
	} 
function validate_email(ob)  //For specific Email Style.
    {
          var emailad;
  emailad=ob;
  var exclude=/[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
  var check=/@[\w\-]+\./;
  var checkend=/\.[a-zA-Z]{2,4}$/;
      if(((emailad.search(exclude) != -1)||(emailad.search(check)) == -1)||(emailad.search(checkend) == -1))
   {
    return false;
   }
  else 
    return true;
    }
    
    
///To validate a general string 
function ValidateString(name)
{
    var strName=name;
	if (strName.charAt(0)==" ")
	{
	    return false;
	}
		
    var cntr;  
    var Pluscntr;
    Pluscntr=0;
    var phstr=name;
    for(cntr=0;cntr<phstr.length;cntr++)
    {
				
        if(phstr.charAt(cntr)=="_")
        {
	        Pluscntr++;
        }
    }
    if(Pluscntr==phstr.length)
    {
	
    return false;
    }

    var cntr;
    var Pluscntr;
    Pluscntr=0;
    var phstr=name;
    for(cntr=0;cntr<phstr.length;cntr++)
    {
				
        if(phstr.charAt(cntr)=="_")
        {
	        Pluscntr++;
        }
    }
    if(Pluscntr==phstr.length)
    {
	
    return false;
    }
		
        var str=name
        var pos
	         pos=str.indexOf("�")
	        if(pos>=0)
		        {
				
		        return false; 	
		        }
			
	         pos=str.indexOf("'")
	        if(pos>=0)
		        {
				
		        return false; 		
		        }
			
	       pos=str.indexOf("\"")
	        if(pos>=0)
		        {
				
		        return false; 		
		        }
			
	         pos=str.indexOf("<")
	        if(pos>=0)
		        {
				
		        return false;	
		        }
			
	        pos=str.indexOf(">")
	        if(pos>=0)
		        {
				
		        return false;	
		        }
			
	         pos=str.indexOf("%")
	        if(pos>=0)
		        {
				
		        return false;	
		        }
		    pos=str.indexOf("*")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		     pos=str.indexOf("~")
	        if(pos>=0)
		        {
				
		        return false;	
		        } 
		    pos=str.indexOf('"')
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		    pos=str.indexOf("#")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		   pos=str.indexOf("@")
	        if(pos>=0)
		        {
				
		        return false;	
		        } 
		  pos=str.indexOf("+")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		   pos=str.indexOf("=")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		    pos=str.indexOf("^")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		     var pos=str.indexOf("!")
	        if(pos>=0)
		        {
				
		        return false;	
		        }
		      var pos=str.indexOf("$")
	        if(pos>=0)
		        {
				
		        return false;	
		        }                                             
		}

//******************************************************************
//
//Purpose	: for Right Trim
//Parameters	: Control Value
//Return Value	: Control Value With Rtrimming
//Example for a : rtrim(Form1.TextBox1.Value)
//
//********************************************************************

		function rtrim(argvalue) 
		{

		  while (1)
		  {
		    if (argvalue.substring(argvalue.length - 1, argvalue.length) != " ")
		      break;

  		     argvalue = argvalue.substring(0, argvalue.length - 1);
		  }

		   return argvalue;
		 }    

//******************************************************************
//
//Purpose	: for Left Trim
//Parameters	: Control Value
//Return Value	: Control Value With Lefttrimming
//Example for a : ltrim(Form1.TextBox1.Value)
//
//********************************************************************

	
		function ltrim(argvalue) 
		{
	
		  while (1)
		   {
		  	  if (argvalue.substring(0, 1) != " ")
			      break;

			    argvalue = argvalue.substring(1, argvalue.length);
		   }

		  return argvalue;
		}    
     

//***********************************************************************************
//
//Purpose	: for Trimming
//Parameters	: Control Value
//Return Value	: Control Value With trimming
//Example for a : trims(Form1.TextBox1.Value)
//
//************************************************************************************

		function trims(argvalue) 
		{
		  var tmpstr = ltrim(argvalue);

		  return rtrim(tmpstr);
 
		}
     

//***********************************************************************************
//
//Purpose	    : for the multiple selection of grid Items
//Parameters	: Control Value
//Return Value	: 
//
//************************************************************************************		

		
		
function check_uncheck(Val,AllCtrl,IndCtrl)
{
  var ValChecked = Val.checked;
  var ValId = Val.id;
  var frm = document.forms[0];
  // Loop through all elements
  for (i = 0; i < frm.length; i++)
  {
    // Look for Header Template's Checkbox
    //As we have not other control other than checkbox we just check following statement
    if (this != null)
    {
      if (ValId.indexOf(AllCtrl) !=  - 1)
      {
        // Check if main checkbox is checked,
        // then select or deselect datagrid checkboxes
        
        if (ValChecked)
          frm.elements[i].checked = true;
        else
          frm.elements[i].checked = false;
      }
      else if (ValId.indexOf(IndCtrl) !=  - 1)
      {
        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
        if (frm.elements[i].checked == false)
          frm.elements[1].checked = false;
      }
    } // if
  } // for
} // function
function CheckChanged(AllCtrl,IndCtrl)	///This function is to Remove the header checkbox selection when column check box is unselected
	{
		var frm = document.forms[0];
		var boolAllChecked;
		boolAllChecked=true;
		for(i=0;i< frm.length;i++)
		{	
		e=frm.elements[i];	
		if ( e.type=='checkbox' && e.name.indexOf(IndCtrl) != -1 )
			if(e.checked== false)	
			{ 
				boolAllChecked=false;
				break;	
			}
		}
		for(i=0;i< frm.length;i++)
		{
		e=frm.elements[i];	
			if ( e.type=='checkbox' && e.name.indexOf(AllCtrl) != -1 )
			{
			if( boolAllChecked==false)
				e.checked= false ;		
			else
				e.checked= true;
				break;
			}
		}
   }
   
  function validate()  //This function is to show alert if users clicks on proceed button 
	                      //without selecting atleast one Item from Table to Delete.
	     {
	     var frm = document.forms[0];
	     
			k =0;         
			for (i = 0; i < frm.length; i++)
			{
				if (frm.elements[i].type == "checkbox")
				{  
	             
					if (frm.elements[i].checked == true)
					{   
						k++;
					}
	            
				}
			}
			if(k<1)
			{
			alert("Please select atleast one Record item.");  
			return false;
			}
			else
			{
			  var ans;
	          ans=window.confirm("Are you sure that you want to delete");
				if(ans==true)
				{
				  return true;
				}
				else
				{
				    for (i = 0; i < frm.length; i++)
			        {
				        if (frm.elements[i].type == "checkbox")
				        {  
        	             
					        if (frm.elements[i].checked == true)
					        {   
						        frm.elements[i].checked = false;
					        }
        	            
				        }
			        }
				  return false;
				}
			}
         return;
	    }
	    
	    
	    
	    
//******************************************************************
//
//Purpose	: for checkDecimals in number
//Parameters	: Control , Decimals count
//Return Value	: valid/error msg
//Example for a : checkDecimals(Form1.TextBox1,2)
//
//******************************************************************
function checkDecimals(ctrlName,Quantity_Amt,DecimalCnt)
			{        
				var msg="";  
					if(ctrlName.value.length==0)
					{ 	
						msg="valid"
						return msg;
					}

					//valid 1st character digit or not       

					if(ctrlName.value.length==1)
					if(ctrlName.value.charAt(0)==".")
					{
						msg="Invalid amount"
						return msg;
					}


					cntDecimals = 0;     
					for(i=0;i<ctrlName.value.length;i++)
					{
						if(ctrlName.value.charAt(i)==".")
							cntDecimals = parseInt(cntDecimals) +1;
					
   					 } 

					 if(cntDecimals>1)
					 {
					      msg="Only One Decimal Should be Accepted"
				              return msg;
					 }
	
					 var decPos=0

					decPos=ctrlName.value.indexOf(".")
 
					if(decPos>=0)				
					{	//Accepting after decimal 2 digits only
					 	if(parseInt(ctrlName.value.length)-parseInt(decPos)>DecimalCnt+1)	
					 	{
							msg="Only " + DecimalCnt + " digists After Decimal"
							return msg;
						}
						if(Quantity_Amt=="")
						{
							//Accepting last digit only 0 or 5
							if(parseInt(ctrlName.value.substring(ctrlName.value.length-1))!=0 && parseInt(ctrlName.value.substring(ctrlName.value.length-1))!=5)
							{
								msg= " Paise " + ctrlName.value.substring(ctrlName.value.length-1) + " Not Allowed"
								return msg;
							}
						}			
					}
					if(validate_currency(ctrlName) == false)	
					{
					   msg = " Enter Valid Currency";
					   return msg;
					}
				return "valid"       				  	
			}
//******************************************************************
//
//Purpose	: for validate currency
//Parameters	: Control 
//Return Value	: true/false
//Example for a : validate_currency(Form1.TextBox1)
//
//********************************************************************

function validate_currency(ob)
{	   

	var str	   	
	s=ob.value
	str=""
	bag="0123456789."
	for (v = 0; v < s.length; v++)
	{   
    	c = s.charAt(v)
	    if (bag.indexOf(c) == -1) 
			str=1
	}
	if(str==1)
	{
		ob.focus();
		return false;
	}
	else
		return true
}
//******************************************************************
//
//Purpose	: for validate Time
//Parameters	: Control 
//Return Value	: true/false
//Example for a : validate_Time(Form1.TextBox1)
//
//********************************************************************


  function validate_Time(ob)
        {	   
            
	        var str	   	
	        s=ob.value
	        str=""
	        bag="0123456789:"
	        for (v = 0; v < s.length; v++)
	        {   
    	        c = s.charAt(v)
	            if (bag.indexOf(c) == -1) 
			        str=1
	        }
	        if(str==1)
	        {
		        ob.focus();
		        return false;
	        }
	        else
		        return true
        }
      
//******************************************************************
//
//Purpose	: for Time Validation in number
//Parameters	: Control
//Return Value	: true/false
//Example for a : validateTime(Form1.TextBox1)
//
//******************************************************************
function validateTime(CtrlName)
 {
  var strval = CtrlName.value;
  var strval1;
  if(strval.length == 0 || strval == "")
  {
   return true;
  }
  //minimum lenght is 6. example 1:2 AM
  if(strval.length < 6)
  {
   alert("Invalid time, Time format should be HH:MM AM/PM");
   return false;
  }
  
  //Maximum length is 8. example 10:45 AM
  if(strval.lenght > 8)
  {
   alert("invalid time, Time format should be HH:MM AM/PM");
   return false;
  }
  
  //Removing all space
  strval = trimAllSpace(strval); 
  
  //Checking AM/PM
  if(strval.charAt(strval.length - 1) != "M" && strval.charAt(strval.length - 1) != "m")
  {
   alert("Invalid time, Time should be end with AM or PM");
   return false;
   
  }
  else if(strval.charAt(strval.length - 2) != 'A' && strval.charAt(strval.length - 2) != 'a' && strval.charAt(strval.length - 2) != 'p' && strval.charAt(strval.length - 2) != 'P')
  {
   alert("Invalid time, Time should be end with AM or PM");
   return false;
   
  }
  
  //Give one space before AM/PM
  
  strval1 =  strval.substring(0,strval.length - 2);
  strval1 = strval1 + ' ' + strval.substring(strval.length - 2,strval.length)
  
  strval = strval1;
      
  var pos1 = strval.indexOf(':');
  CtrlName.value = strval;
  
  if(pos1 < 0 )
  {
   alert("Invlalid time, A colon(:) is missing between hour and minute");
   return false;
  }
  else if(pos1 > 2 || pos1 < 1)
  {
   alert("Invalid time, Time format should be HH:MM AM/PM");
   return false;
  }
  
  //Checking hours
  var horval =  trimString(strval.substring(0,pos1));
   
  if(horval == -100)
  {
   alert("Invalid time, Hour should contain only integer value (0-11)");
   return false;
  }
      
  if(horval > 12)
  {
   alert("Invalid time, Hour can not be greater that 12");
   return false;
  }
  else if(horval < 0)
  {
   alert("Invalid time, Hour can not be hours less than 0");
   return false;
  }
  //Completes checking hours.
  
  //Checking minutes.
  var minval =  trimString(strval.substring(pos1+1,pos1 + 3));
  
  if(minval == -100)
  {
   alert("Invalid time, Minute should have only integer value (0-59)");
   return false;
  }
    
  if(minval > 59)
  {
     alert("Invalid time, Minute can not be more than 59");
     return false;
  }   
  else if(minval < 0)
  {
   alert("Invalid time, Minute can not be less than 0");
   return false;
  }
   
  //Checking minutes completed.  
  
  //Checking one space after the mintues 
  minpos = pos1 + minval.length + 1;
  if(strval.charAt(minpos) != ' ')
  {
   alert("Invalid time, Space missing after minute. Time should have HH:MM AM/PM format");
   return false;
  }
  return true;
 }
function trimAllSpace(str)
 {
  var str1 = ''; 
  var i = 0;
   while(i != str.length)
    {
     if(str.charAt(i) != ' ')
     str1 = str1 + str.charAt(i); i ++; 
    } 
    return str1; 
 }
function trimString(str) 
{ 
 var str1 = '';
 var i = 0; 
 while ( i != str.length)
  { 
  if(str.charAt(i) != ' ') 
  str1 = str1 + str.charAt(i); i++;
  } 
  var retval = IsNumeric(str1);
   if(retval == false) 
   return -100; 
   else return str1; 
}
function IsNumeric(strString)  
 { 
   var strValidChars = "0123456789";
   var strChar; 
   var blnResult = true;
   // test strString consists of valid characters listed above
     if (strString.length == 0) return false;
      for (i = 0; i < strString.length && blnResult == true; i++)
       { 
       strChar = strString.charAt(i); 
       if (strValidChars.indexOf(strChar) == -1)
        {
         blnResult = false; 
         } 
       }
       return blnResult;
 }

function dateSwap(Date)
{
  var datePartDt; 
  datePartDt  = Date.split("index.html");
  Date = datePartEdt[1]+"/"+datePartEdt[0]+"/"+datePartEdt[2];
  return Date;
}


//******************************************************************
//
//Purpose	: for checkDecimals in number
//Parameters	: Control , Decimals count
//Return Value	: valid/error msg
//Example for a : checkDecimals(Form1.TextBox1,2)
//
//******************************************************************
function checkDecimals(ctrlName,Quantity_Amt,DecimalCnt)
			{        
				var msg="";  
					if(ctrlName.value.length==0)
					{ 	
						msg="valid"
						return msg;
					}

					//valid 1st character digit or not       

					if(ctrlName.value.length==1)
					if(ctrlName.value.charAt(0)==".")
					{
						msg='Invalid format'
						return msg;
					}


					cntDecimals = 0;     
					for(i=0;i<ctrlName.value.length;i++)
					{
						if(ctrlName.value.charAt(i)==".")
							cntDecimals = parseInt(cntDecimals) +1;
					
   					 } 

					 if(cntDecimals>1)
					 {
					      msg="Only One Decimal Should be Accepted"
				              return msg;
					 }
	
					 var decPos=0

					decPos=ctrlName.value.indexOf(".")
 
					if(decPos>=0)				
					{	//Accepting after decimal 2 digits only
					 	if(parseInt(ctrlName.value.length)-parseInt(decPos)>DecimalCnt+1)	
					 	{
							msg="Only " + DecimalCnt + " digists After Decimal"
							return msg;
						}
//						if(Quantity_Amt=="")
//						{
//							//Accepting last digit only 0 or 5
//							if(parseInt(ctrlName.value.substring(ctrlName.value.length-1))!=0 && parseInt(ctrlName.value.substring(ctrlName.value.length-1))!=5)
//							{
//								msg= " Paise " + ctrlName.value.substring(ctrlName.value.length-1) + " Not Allowed"
//								return msg;
//							}
//						}			
					}
					if(validate_currency(ctrlName) == false)	
					{
					   msg = 'Invalid format';
					   return msg;
					}
				return "valid"       				  	
			}
			
			//***********************************************************************************
//
//Purpose	    : for Validate Address format
//Parameters	: Control  
//Return Value	: true/false
//Example for a : validate_address(Form1.TextBox1)
//
//************************************************************************************


function validate_address(ob)
{		
    
	s=ob.value
    var str
	bag="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-#/_,. &!@#$%&*()_+|~{}:?./-;'"
	for (i = 0; i < s.length; i++)
	{   
		if((s.charCodeAt(i) != 13) && (s.charCodeAt(i) != 10))
		{
		c = s.charAt(i)
		if (bag.indexOf(c) == -1) 
			str=1
		}
	}	
	if(str==1)
	{
		ob.focus();
		return false;
	}
	else
		return true
}




   
///To validate SMS text//~ ^ { } [ ] | \ 
function ValidateSMSString(name)
{
    var strName=name;
	if (strName.charAt(0)==" ")
	{
	    return false;
	}
		
    		
        var str=name
        var pos

			
	       pos=str.indexOf("\"")
	        if(pos>=0)
		        {
				
		        return false; 		
		        }
			
	         pos=str.indexOf("<")
	        if(pos>=0)
		        {
				
		        return false;	
		        }
			
	        pos=str.indexOf(">")
	        if(pos>=0)
		        {
				
		        return false;	
		        }
			
            pos=str.indexOf("~")
	        if(pos>=0)
		        {
				
		        return false;	
		        } 
		  pos=str.indexOf("{")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		   pos=str.indexOf("}")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		    pos=str.indexOf("^")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		    var pos=str.indexOf("[")
	        if(pos>=0)
		        {
				
		        return false;	
		        }
		    var pos=str.indexOf("]")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		    var pos=str.indexOf("|")
	        if(pos>=0)
		        {
				
		        return false;	
		        }  
		    var pos=str.indexOf("+")
	        if(pos>=0)
		        {
				
		        return false;	
		        } 
		                                                 
		}