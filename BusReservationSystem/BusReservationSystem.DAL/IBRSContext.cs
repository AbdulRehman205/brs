﻿using BusReservationSystem.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusReservationSystem.DAL
{
    public interface IBRSContext
    {

        IDbSet<BookDetail> BookDetails { get; set; }
        IDbSet<CustomerDetail> CustomerDetails { get; set; }
        IDbSet<DriverDetail> DriverDetails { get; set; }
        IDbSet<PassengerDetail> PassengerDetails { get; set; }
        IDbSet<RouteDetail> RouteDetails { get; set; }
        IDbSet<TicketDetail> TicketDetails { get; set; }
        IDbSet<TicketFare> TicketFares { get; set; }
        IDbSet<TimeTable> TimeTables { get; set; }
        IDbSet<UserLogin> UserLogins { get; set; }

        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        void SetModified(object entity);
        int SaveChanges();
    }
}
