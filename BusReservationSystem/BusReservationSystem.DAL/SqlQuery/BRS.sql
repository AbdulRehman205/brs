USE [master]
GO
/****** Object:  Database [BRS]    Script Date: 11/01/2017 23:07:26 ******/
CREATE DATABASE [BRS] ON  PRIMARY 
( NAME = N'BRS', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\BRS.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BRS_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\BRS_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BRS] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BRS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BRS] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [BRS] SET ANSI_NULLS OFF
GO
ALTER DATABASE [BRS] SET ANSI_PADDING OFF
GO
ALTER DATABASE [BRS] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [BRS] SET ARITHABORT OFF
GO
ALTER DATABASE [BRS] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [BRS] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [BRS] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [BRS] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [BRS] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [BRS] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [BRS] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [BRS] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [BRS] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [BRS] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [BRS] SET  DISABLE_BROKER
GO
ALTER DATABASE [BRS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [BRS] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [BRS] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [BRS] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [BRS] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [BRS] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [BRS] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [BRS] SET  READ_WRITE
GO
ALTER DATABASE [BRS] SET RECOVERY SIMPLE
GO
ALTER DATABASE [BRS] SET  MULTI_USER
GO
ALTER DATABASE [BRS] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [BRS] SET DB_CHAINING OFF
GO
USE [BRS]
GO
/****** Object:  Table [dbo].[TicketDetails]    Script Date: 11/01/2017 23:07:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketDetails](
	[TicketId] [int] IDENTITY(1,1) NOT NULL,
	[ArrivalStation] [nvarchar](50) NOT NULL,
	[ArrivalTime] [datetime] NOT NULL,
	[DepartureStation] [nvarchar](50) NOT NULL,
	[DepartureTime] [datetime] NOT NULL,
	[Distance] [int] NOT NULL,
	[Fare] [money] NOT NULL,
	[PaymentMode] [nvarchar](50) NOT NULL,
	[ViaStation] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [date] NULL,
 CONSTRAINT [PK_TicketDetails] PRIMARY KEY CLUSTERED 
(
	[TicketId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RouteDetails]    Script Date: 11/01/2017 23:07:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RouteDetails](
	[RouteId] [int] IDENTITY(1,1) NOT NULL,
	[RouteName] [nvarchar](50) NOT NULL,
	[Arrival] [nvarchar](50) NOT NULL,
	[Departure] [nvarchar](50) NOT NULL,
	[Distance] [money] NULL,
	[ViaStation] [nvarchar](50) NULL,
	[BusNumber] [nvarchar](50) NULL,
	[BusPlateNumber] [nvarchar](50) NOT NULL,
	[BusType] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [date] NULL,
 CONSTRAINT [PK_RouteDetails] PRIMARY KEY CLUSTERED 
(
	[RouteName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PassengerDetails]    Script Date: 11/01/2017 23:07:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PassengerDetails](
	[PassengerId] [int] IDENTITY(1,1) NOT NULL,
	[PassengerName] [nvarchar](50) NOT NULL,
	[Mobile] [int] NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Age] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [date] NULL,
 CONSTRAINT [PK_PassengerDetails] PRIMARY KEY CLUSTERED 
(
	[PassengerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerDetails]    Script Date: 11/01/2017 23:07:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerDetails](
	[CustomerId] [int] NOT NULL,
	[SubAgent] [nvarchar](50) NULL,
	[SeatNumber] [nvarchar](50) NULL,
	[Boarding] [nvarchar](50) NULL,
	[DropOff] [nvarchar](50) NULL,
	[CustomerName] [nvarchar](50) NULL,
	[Gender] [nchar](10) NULL,
	[Age] [int] NULL,
	[Mobile] [int] NULL,
	[Address] [nvarchar](50) NULL,
	[Fare] [money] NULL,
	[BookingDate] [datetime] NULL,
	[Comments] [nvarchar](50) NULL,
	[NumberOfSeats] [int] NULL,
	[PaymentType] [nchar](10) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [date] NULL,
 CONSTRAINT [PK_CustomerDetails] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLogin]    Script Date: 11/01/2017 23:07:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLogin](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[EmailId] [nvarchar](50) NOT NULL,
	[UserRole] [nchar](10) NOT NULL,
	[IsActive] [nchar](10) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [date] NULL,
 CONSTRAINT [PK_UserLogin] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimeTable]    Script Date: 11/01/2017 23:07:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeTable](
	[TimeTableId] [int] IDENTITY(1,1) NOT NULL,
	[ArrivalStation] [nvarchar](50) NOT NULL,
	[ArrivalTime] [datetime] NOT NULL,
	[DepartureStation] [nvarchar](50) NOT NULL,
	[DeparturreTime] [datetime] NOT NULL,
	[Distance] [int] NOT NULL,
	[TotalJourneyTime] [money] NULL,
	[Fare] [money] NOT NULL,
	[RouteName] [nvarchar](50) NOT NULL,
	[DelayInformation] [nvarchar](50) NULL,
	[Mobile] [int] NULL,
	[ViaStation] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [date] NULL,
 CONSTRAINT [PK_TimeTable] PRIMARY KEY CLUSTERED 
(
	[TimeTableId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketFare]    Script Date: 11/01/2017 23:07:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketFare](
	[TicketFareId] [int] IDENTITY(1,1) NOT NULL,
	[ArrivalStation] [nvarchar](50) NOT NULL,
	[DepartureStation] [nvarchar](50) NOT NULL,
	[Distance] [money] NOT NULL,
	[Fare] [money] NOT NULL,
	[RouteName] [nvarchar](50) NOT NULL,
	[FareDate] [datetime] NULL,
	[TicketType] [nchar](10) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [date] NULL,
 CONSTRAINT [PK_TicketFare] PRIMARY KEY CLUSTERED 
(
	[TicketFareId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BookDetails]    Script Date: 11/01/2017 23:07:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookDetails](
	[BookingId] [int] IDENTITY(1,1) NOT NULL,
	[PassengerId] [int] NOT NULL,
	[ArrivalStation] [nvarchar](50) NOT NULL,
	[BookingDate] [datetime] NOT NULL,
	[BusType] [nvarchar](50) NOT NULL,
	[DepartureStation] [nvarchar](50) NOT NULL,
	[Distance] [money] NOT NULL,
	[BoardingPoint] [nvarchar](50) NOT NULL,
	[JourneyDate] [datetime] NOT NULL,
	[SMSStatus] [nvarchar](50) NOT NULL,
	[EmailStatus] [nvarchar](50) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[Reason] [nvarchar](50) NULL,
	[Fare] [money] NOT NULL,
	[AditionalFare] [money] NULL,
	[GST] [money] NOT NULL,
	[ServiceCharge] [money] NULL,
	[Commision] [money] NOT NULL,
	[RouteName] [nvarchar](50) NOT NULL,
	[SeatNumber] [nvarchar](50) NOT NULL,
	[ViaStation] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_BookDetails] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DriverDetails]    Script Date: 11/01/2017 23:07:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DriverDetails](
	[DriverId] [int] IDENTITY(1,1) NOT NULL,
	[RouteName] [nvarchar](50) NOT NULL,
	[DriverName] [nvarchar](50) NOT NULL,
	[MobileNumber] [int] NOT NULL,
	[LicenceNumber] [nvarchar](50) NOT NULL,
	[AlternateNumber] [int] NULL,
	[Address] [nvarchar](50) NOT NULL,
	[Address1] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [date] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [date] NULL,
 CONSTRAINT [PK_DriverDetails] PRIMARY KEY CLUSTERED 
(
	[DriverId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_TimeTable_RouteDetails]    Script Date: 11/01/2017 23:07:29 ******/
ALTER TABLE [dbo].[TimeTable]  WITH CHECK ADD  CONSTRAINT [FK_TimeTable_RouteDetails] FOREIGN KEY([RouteName])
REFERENCES [dbo].[RouteDetails] ([RouteName])
GO
ALTER TABLE [dbo].[TimeTable] CHECK CONSTRAINT [FK_TimeTable_RouteDetails]
GO
/****** Object:  ForeignKey [FK_TicketFare_RouteDetails]    Script Date: 11/01/2017 23:07:29 ******/
ALTER TABLE [dbo].[TicketFare]  WITH CHECK ADD  CONSTRAINT [FK_TicketFare_RouteDetails] FOREIGN KEY([RouteName])
REFERENCES [dbo].[RouteDetails] ([RouteName])
GO
ALTER TABLE [dbo].[TicketFare] CHECK CONSTRAINT [FK_TicketFare_RouteDetails]
GO
/****** Object:  ForeignKey [FK_BookDetails_PassengerDetails]    Script Date: 11/01/2017 23:07:29 ******/
ALTER TABLE [dbo].[BookDetails]  WITH CHECK ADD  CONSTRAINT [FK_BookDetails_PassengerDetails] FOREIGN KEY([PassengerId])
REFERENCES [dbo].[PassengerDetails] ([PassengerId])
GO
ALTER TABLE [dbo].[BookDetails] CHECK CONSTRAINT [FK_BookDetails_PassengerDetails]
GO
/****** Object:  ForeignKey [FK_DriverDetails_DriverDetails]    Script Date: 11/01/2017 23:07:29 ******/
ALTER TABLE [dbo].[DriverDetails]  WITH CHECK ADD  CONSTRAINT [FK_DriverDetails_DriverDetails] FOREIGN KEY([RouteName])
REFERENCES [dbo].[RouteDetails] ([RouteName])
GO
ALTER TABLE [dbo].[DriverDetails] CHECK CONSTRAINT [FK_DriverDetails_DriverDetails]
GO
