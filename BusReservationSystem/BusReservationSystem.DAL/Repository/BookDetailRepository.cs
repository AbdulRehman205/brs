﻿using BusReservationSystem.DAL.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusReservationSystem.DAL.Repository
{
    public class BookDetailRepository : Repository<Models.BookDetail>, IBookDetailRepository
    {
        public BookDetailRepository(IBRSContext bsrContext):base(bsrContext)
        {

        }
    }
}
