﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;

namespace BusReservationSystem.DAL
{
    public class Repository<T> : IRepository<T> where T : class
    {
        // protected TestContext TestContext;
        // public DbContext Context { get { return ContextContainer.Ctx; } }
        // private IDbSet<T> DbSet { get { return ContextContainer.Ctx.Set<T>(); } }
        // private DbContextTransaction _transaction = null;
        // public Contracts.IContextContainer ContainerContext;
        //private readonly IDbSet<T> DbSet { get;set; }

        // protected DbContext Context;
        protected readonly IDbSet<T> DbSet;

        protected int MaxPage { get; set; }

        private IBRSContext Context = null;


        //public Repository()
        //{
        //    this.SMSContext = new SMSContext();
        //    if (Data.ContextContainer.Ctx == null)
        //    {
        //        Data.ContextContainer.Ctx = this.SMSContext;
        //        // this.Context = Data.ContextContainer.Ctx;
        //    }
        //    // this.Context = Data.ContextContainer.Ctx;
        //    // this.ContainerContext.Context = this.SMSContext;
        //    // this.ContainerContext = new Data.ContextContainer();
        //    // this.DbSet = this.Context.Set<T>();
        //    //this.Context.Configuration.AutoDetectChangesEnabled = false;
        //    // this.Context.BulkInsert<T>()
        //    this.MaxPage = int.Parse(ConfigurationManager.AppSettings["MaxPage"].ToString());
        //}

        public Repository(IBRSContext testContext)
        {

            //Context = context;
            // Context = (DbContext)testContext;

            this.Context = testContext;
            this.DbSet = testContext.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return this.DbSet.AsQueryable();
        }

        public virtual IQueryable<T> All()
        {
            return this.DbSet.AsQueryable();
        }

        public virtual bool Contains(Expression<Func<T, bool>> predicate)
        {

            return this.DbSet.Count(predicate) > 0;
        }

        public virtual IQueryable<T> Filter(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate).AsQueryable<T>();
        }

        public virtual T Find(Expression<Func<T, bool>> predicate)
        {
            return this.DbSet.FirstOrDefault(predicate);
        }

        public virtual int Insert(T entity)
        {
            this.DbSet.Add(entity);

            return this.Context.SaveChanges();
        }

        public virtual int Delete(T entity)
        {
            this.DbSet.Remove(entity);

            return this.Context.SaveChanges();
        }

        public virtual int Delete(Expression<Func<T, bool>> predicate)
        {
            var ObjectsToDelete = this.Filter(predicate);

            foreach (var ObjectToDelete in ObjectsToDelete)
            {
                this.DbSet.Remove(ObjectToDelete);
            }

            return this.Context.SaveChanges();
        }

        public virtual int Update(T entity)
        {
            this.Context.SetModified(entity);

            return this.Context.SaveChanges();
        }

        public virtual TEntity FindSingleBy<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            if (predicate != null)
            {
                return this.Context.Set<TEntity>().Where(predicate).SingleOrDefault();
            }
            else
            {
                throw new ArgumentNullException("Predicate value must be passed to FindSingleBy<T>.");
            }
        }

        public virtual T GetById(object id)
        {
            return this.DbSet.Find(id);
        }

        public virtual IQueryable<T> PageFilter(Expression<Func<T, bool>> filter, out int total, int page = 0, int size = 50)
        {
            int skipCount = page * size;
            var resetSet = filter != null ? this.DbSet.Where(filter).AsQueryable() : this.DbSet.AsQueryable();
            resetSet = skipCount == 0 ? resetSet.Take(size) : resetSet.Skip(skipCount).Take(size);
            total = resetSet.Count();
            return resetSet.AsQueryable();
        }

        public virtual IQueryable<T> PageFilter(IQueryable<T> entity, int pageNumber, int pageSize, ref int totalRecords, ref bool hasMoreRecords)
        {
            int skipCount = pageNumber * pageSize;

            var pageQuery = entity.Skip(skipCount).Take(pageSize);
            totalRecords = this.DbSet.Count();
            hasMoreRecords = (pageNumber * pageSize + 1) < totalRecords;
            return pageQuery;
        }

        public virtual IEnumerable<T> PageFilter(Func<T, int> orderQuery, int pageNumber, int pageSize, ref int totalRecords, ref bool hasMoreRecords)
        {
            int skipCount = pageNumber * pageSize;

            var pageQuery = this.GetAll().OrderBy(orderQuery).Skip(skipCount).Take(pageSize);
            totalRecords = this.GetAll().Count();
            hasMoreRecords = ((pageNumber + 1) * pageSize) < totalRecords;
            return pageQuery;
        }

        public static string GetKeyField(Type type)
        {
            var allProperties = type.GetProperties();

            var keyProperty = allProperties.SingleOrDefault(p => p.IsDefined(typeof(System.ComponentModel.DataAnnotations.KeyAttribute), true));

            return keyProperty != null ? keyProperty.Name : null;
        }


        public virtual T Find(params object[] keys)
        {
            return this.DbSet.Find(keys);
        }


        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
