using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BusReservationSystem.DAL.Models.Mapping
{
    public class CustomerDetailMap : EntityTypeConfiguration<CustomerDetail>
    {
        public CustomerDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.CustomerId);

            // Properties
            this.Property(t => t.CustomerId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SubAgent)
                .HasMaxLength(50);

            this.Property(t => t.SeatNumber)
                .HasMaxLength(50);

            this.Property(t => t.Boarding)
                .HasMaxLength(50);

            this.Property(t => t.DropOff)
                .HasMaxLength(50);

            this.Property(t => t.CustomerName)
                .HasMaxLength(50);

            this.Property(t => t.Gender)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.Address)
                .HasMaxLength(50);

            this.Property(t => t.Comments)
                .HasMaxLength(50);

            this.Property(t => t.PaymentType)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UpdatedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CustomerDetails");
            this.Property(t => t.CustomerId).HasColumnName("CustomerId");
            this.Property(t => t.SubAgent).HasColumnName("SubAgent");
            this.Property(t => t.SeatNumber).HasColumnName("SeatNumber");
            this.Property(t => t.Boarding).HasColumnName("Boarding");
            this.Property(t => t.DropOff).HasColumnName("DropOff");
            this.Property(t => t.CustomerName).HasColumnName("CustomerName");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.Age).HasColumnName("Age");
            this.Property(t => t.Mobile).HasColumnName("Mobile");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Fare).HasColumnName("Fare");
            this.Property(t => t.BookingDate).HasColumnName("BookingDate");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.NumberOfSeats).HasColumnName("NumberOfSeats");
            this.Property(t => t.PaymentType).HasColumnName("PaymentType");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy");
            this.Property(t => t.UpdatedOn).HasColumnName("UpdatedOn");
        }
    }
}
