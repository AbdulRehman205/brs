using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BusReservationSystem.DAL.Models.Mapping
{
    public class TicketFareMap : EntityTypeConfiguration<TicketFare>
    {
        public TicketFareMap()
        {
            // Primary Key
            this.HasKey(t => t.TicketFareId);

            // Properties
            this.Property(t => t.ArrivalStation)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DepartureStation)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.RouteName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.TicketType)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UpdatedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TicketFare");
            this.Property(t => t.TicketFareId).HasColumnName("TicketFareId");
            this.Property(t => t.ArrivalStation).HasColumnName("ArrivalStation");
            this.Property(t => t.DepartureStation).HasColumnName("DepartureStation");
            this.Property(t => t.Distance).HasColumnName("Distance");
            this.Property(t => t.Fare).HasColumnName("Fare");
            this.Property(t => t.RouteName).HasColumnName("RouteName");
            this.Property(t => t.FareDate).HasColumnName("FareDate");
            this.Property(t => t.TicketType).HasColumnName("TicketType");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy");
            this.Property(t => t.UpdatedOn).HasColumnName("UpdatedOn");

            // Relationships
            this.HasRequired(t => t.RouteDetail)
                .WithMany(t => t.TicketFares)
                .HasForeignKey(d => d.RouteName);

        }
    }
}
