using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BusReservationSystem.DAL.Models.Mapping
{
    public class DriverDetailMap : EntityTypeConfiguration<DriverDetail>
    {
        public DriverDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.DriverId);

            // Properties
            this.Property(t => t.RouteName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DriverName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LicenceNumber)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Address1)
                .HasMaxLength(50);

            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UpdatedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DriverDetails");
            this.Property(t => t.DriverId).HasColumnName("DriverId");
            this.Property(t => t.RouteName).HasColumnName("RouteName");
            this.Property(t => t.DriverName).HasColumnName("DriverName");
            this.Property(t => t.MobileNumber).HasColumnName("MobileNumber");
            this.Property(t => t.LicenceNumber).HasColumnName("LicenceNumber");
            this.Property(t => t.AlternateNumber).HasColumnName("AlternateNumber");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Address1).HasColumnName("Address1");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy");
            this.Property(t => t.UpdatedOn).HasColumnName("UpdatedOn");

            // Relationships
            this.HasRequired(t => t.RouteDetail)
                .WithMany(t => t.DriverDetails)
                .HasForeignKey(d => d.RouteName);

        }
    }
}
