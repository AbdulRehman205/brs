using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BusReservationSystem.DAL.Models.Mapping
{
    public class BookDetailMap : EntityTypeConfiguration<BookDetail>
    {
        public BookDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.BookingId);

            // Properties
            this.Property(t => t.ArrivalStation)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.BusType)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DepartureStation)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.BoardingPoint)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SMSStatus)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.EmailStatus)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Status)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Reason)
                .HasMaxLength(50);

            this.Property(t => t.RouteName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.SeatNumber)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ViaStation)
                .HasMaxLength(50);

            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UpdatedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("BookDetails");
            this.Property(t => t.BookingId).HasColumnName("BookingId");
            this.Property(t => t.PassengerId).HasColumnName("PassengerId");
            this.Property(t => t.ArrivalStation).HasColumnName("ArrivalStation");
            this.Property(t => t.BookingDate).HasColumnName("BookingDate");
            this.Property(t => t.BusType).HasColumnName("BusType");
            this.Property(t => t.DepartureStation).HasColumnName("DepartureStation");
            this.Property(t => t.Distance).HasColumnName("Distance");
            this.Property(t => t.BoardingPoint).HasColumnName("BoardingPoint");
            this.Property(t => t.JourneyDate).HasColumnName("JourneyDate");
            this.Property(t => t.SMSStatus).HasColumnName("SMSStatus");
            this.Property(t => t.EmailStatus).HasColumnName("EmailStatus");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Reason).HasColumnName("Reason");
            this.Property(t => t.Fare).HasColumnName("Fare");
            this.Property(t => t.AditionalFare).HasColumnName("AditionalFare");
            this.Property(t => t.GST).HasColumnName("GST");
            this.Property(t => t.ServiceCharge).HasColumnName("ServiceCharge");
            this.Property(t => t.Commision).HasColumnName("Commision");
            this.Property(t => t.RouteName).HasColumnName("RouteName");
            this.Property(t => t.SeatNumber).HasColumnName("SeatNumber");
            this.Property(t => t.ViaStation).HasColumnName("ViaStation");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy");
            this.Property(t => t.UpdatedOn).HasColumnName("UpdatedOn");

            // Relationships
            this.HasRequired(t => t.PassengerDetail)
                .WithMany(t => t.BookDetails)
                .HasForeignKey(d => d.PassengerId);

        }
    }
}
