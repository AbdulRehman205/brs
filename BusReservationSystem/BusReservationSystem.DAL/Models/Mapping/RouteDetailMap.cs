using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BusReservationSystem.DAL.Models.Mapping
{
    public class RouteDetailMap : EntityTypeConfiguration<RouteDetail>
    {
        public RouteDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.RouteName);

            // Properties
            this.Property(t => t.RouteId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.RouteName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Arrival)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Departure)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ViaStation)
                .HasMaxLength(50);

            this.Property(t => t.BusNumber)
                .HasMaxLength(50);

            this.Property(t => t.BusPlateNumber)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.BusType)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UpdatedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("RouteDetails");
            this.Property(t => t.RouteId).HasColumnName("RouteId");
            this.Property(t => t.RouteName).HasColumnName("RouteName");
            this.Property(t => t.Arrival).HasColumnName("Arrival");
            this.Property(t => t.Departure).HasColumnName("Departure");
            this.Property(t => t.Distance).HasColumnName("Distance");
            this.Property(t => t.ViaStation).HasColumnName("ViaStation");
            this.Property(t => t.BusNumber).HasColumnName("BusNumber");
            this.Property(t => t.BusPlateNumber).HasColumnName("BusPlateNumber");
            this.Property(t => t.BusType).HasColumnName("BusType");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy");
            this.Property(t => t.UpdatedOn).HasColumnName("UpdatedOn");
        }
    }
}
