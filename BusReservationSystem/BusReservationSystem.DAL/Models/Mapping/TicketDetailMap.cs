using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BusReservationSystem.DAL.Models.Mapping
{
    public class TicketDetailMap : EntityTypeConfiguration<TicketDetail>
    {
        public TicketDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.TicketId);

            // Properties
            this.Property(t => t.ArrivalStation)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DepartureStation)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.PaymentMode)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ViaStation)
                .HasMaxLength(50);

            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UpdatedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TicketDetails");
            this.Property(t => t.TicketId).HasColumnName("TicketId");
            this.Property(t => t.ArrivalStation).HasColumnName("ArrivalStation");
            this.Property(t => t.ArrivalTime).HasColumnName("ArrivalTime");
            this.Property(t => t.DepartureStation).HasColumnName("DepartureStation");
            this.Property(t => t.DepartureTime).HasColumnName("DepartureTime");
            this.Property(t => t.Distance).HasColumnName("Distance");
            this.Property(t => t.Fare).HasColumnName("Fare");
            this.Property(t => t.PaymentMode).HasColumnName("PaymentMode");
            this.Property(t => t.ViaStation).HasColumnName("ViaStation");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy");
            this.Property(t => t.UpdatedOn).HasColumnName("UpdatedOn");
        }
    }
}
