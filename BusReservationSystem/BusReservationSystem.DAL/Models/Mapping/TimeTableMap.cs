using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BusReservationSystem.DAL.Models.Mapping
{
    public class TimeTableMap : EntityTypeConfiguration<TimeTable>
    {
        public TimeTableMap()
        {
            // Primary Key
            this.HasKey(t => t.TimeTableId);

            // Properties
            this.Property(t => t.ArrivalStation)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DepartureStation)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.RouteName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DelayInformation)
                .HasMaxLength(50);

            this.Property(t => t.ViaStation)
                .HasMaxLength(50);

            this.Property(t => t.CreatedBy)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UpdatedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TimeTable");
            this.Property(t => t.TimeTableId).HasColumnName("TimeTableId");
            this.Property(t => t.ArrivalStation).HasColumnName("ArrivalStation");
            this.Property(t => t.ArrivalTime).HasColumnName("ArrivalTime");
            this.Property(t => t.DepartureStation).HasColumnName("DepartureStation");
            this.Property(t => t.DeparturreTime).HasColumnName("DeparturreTime");
            this.Property(t => t.Distance).HasColumnName("Distance");
            this.Property(t => t.TotalJourneyTime).HasColumnName("TotalJourneyTime");
            this.Property(t => t.Fare).HasColumnName("Fare");
            this.Property(t => t.RouteName).HasColumnName("RouteName");
            this.Property(t => t.DelayInformation).HasColumnName("DelayInformation");
            this.Property(t => t.Mobile).HasColumnName("Mobile");
            this.Property(t => t.ViaStation).HasColumnName("ViaStation");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.UpdatedBy).HasColumnName("UpdatedBy");
            this.Property(t => t.UpdatedOn).HasColumnName("UpdatedOn");

            // Relationships
            this.HasRequired(t => t.RouteDetail)
                .WithMany(t => t.TimeTables)
                .HasForeignKey(d => d.RouteName);

        }
    }
}
