using System;
using System.Collections.Generic;

namespace BusReservationSystem.DAL.Models
{
    public partial class RouteDetail
    {
        public RouteDetail()
        {
            this.DriverDetails = new List<DriverDetail>();
            this.TicketFares = new List<TicketFare>();
            this.TimeTables = new List<TimeTable>();
        }

        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public string Arrival { get; set; }
        public string Departure { get; set; }
        public Nullable<decimal> Distance { get; set; }
        public string ViaStation { get; set; }
        public string BusNumber { get; set; }
        public string BusPlateNumber { get; set; }
        public string BusType { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public virtual ICollection<DriverDetail> DriverDetails { get; set; }
        public virtual ICollection<TicketFare> TicketFares { get; set; }
        public virtual ICollection<TimeTable> TimeTables { get; set; }
    }
}
