using System;
using System.Collections.Generic;

namespace BusReservationSystem.DAL.Models
{
    public partial class CustomerDetail
    {
        public int CustomerId { get; set; }
        public string SubAgent { get; set; }
        public string SeatNumber { get; set; }
        public string Boarding { get; set; }
        public string DropOff { get; set; }
        public string CustomerName { get; set; }
        public string Gender { get; set; }
        public Nullable<int> Age { get; set; }
        public Nullable<int> Mobile { get; set; }
        public string Address { get; set; }
        public Nullable<decimal> Fare { get; set; }
        public Nullable<System.DateTime> BookingDate { get; set; }
        public string Comments { get; set; }
        public Nullable<int> NumberOfSeats { get; set; }
        public string PaymentType { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
