using System;
using System.Collections.Generic;

namespace BusReservationSystem.DAL.Models
{
    public partial class PassengerDetail
    {
        public PassengerDetail()
        {
            this.BookDetails = new List<BookDetail>();
        }

        public int PassengerId { get; set; }
        public string PassengerName { get; set; }
        public int Mobile { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public virtual ICollection<BookDetail> BookDetails { get; set; }
    }
}
