using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using BusReservationSystem.DAL.Models;
using BusReservationSystem.DAL.Models.Mapping;

namespace BusReservationSystem.DAL
{
    public partial class BRSContext : DbContext, IBRSContext
    {
        static BRSContext()
        {
            Database.SetInitializer<BRSContext>(null);
        }

        public BRSContext()
            : base("Name=BRSContext")
        {
        }

        public IDbSet<BookDetail> BookDetails { get; set; }
        public IDbSet<CustomerDetail> CustomerDetails { get; set; }
        public IDbSet<DriverDetail> DriverDetails { get; set; }
        public IDbSet<PassengerDetail> PassengerDetails { get; set; }
        public IDbSet<RouteDetail> RouteDetails { get; set; }
        public IDbSet<TicketDetail> TicketDetails { get; set; }
        public IDbSet<TicketFare> TicketFares { get; set; }
        public IDbSet<TimeTable> TimeTables { get; set; }
        public IDbSet<UserLogin> UserLogins { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BookDetailMap());
            modelBuilder.Configurations.Add(new CustomerDetailMap());
            modelBuilder.Configurations.Add(new DriverDetailMap());
            modelBuilder.Configurations.Add(new PassengerDetailMap());
            modelBuilder.Configurations.Add(new RouteDetailMap());
            modelBuilder.Configurations.Add(new TicketDetailMap());
            modelBuilder.Configurations.Add(new TicketFareMap());
            modelBuilder.Configurations.Add(new TimeTableMap());
            modelBuilder.Configurations.Add(new UserLoginMap());
        }
        public new IDbSet<TEntity> Set<TEntity>()
           where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public void SetModified(object entity)
        {
            base.Entry(entity).State = EntityState.Modified;
        }

        public int SaveChanges()
        {
            return base.SaveChanges();
        }

    }
}
