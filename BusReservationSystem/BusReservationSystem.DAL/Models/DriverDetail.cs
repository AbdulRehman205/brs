using System;
using System.Collections.Generic;

namespace BusReservationSystem.DAL.Models
{
    public partial class DriverDetail
    {
        public int DriverId { get; set; }
        public string RouteName { get; set; }
        public string DriverName { get; set; }
        public int MobileNumber { get; set; }
        public string LicenceNumber { get; set; }
        public Nullable<int> AlternateNumber { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public virtual RouteDetail RouteDetail { get; set; }
    }
}
