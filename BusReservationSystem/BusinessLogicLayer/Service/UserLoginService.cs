﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusReservationSystem.DAL;
using BusReservationSystem.DAL.IRepository;

namespace BusinessLogicLayer.Service
{
    public class UserLoginService : IService.IUserLoginService
    {
        private readonly IUserLoginRepository _userLoginRepository;
        public UserLoginService(IUserLoginRepository userLoginRepository)
        {
            _userLoginRepository = userLoginRepository;
        }
    }
}
