﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Models
{
    public class PassengerDetailModel
    {
        public PassengerDetailModel()
        {
            this.BookDetails = new List<BookDetailModel>();
        }

        public int PassengerId { get; set; }
        public string PassengerName { get; set; }
        public int Mobile { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    
public  List<BookDetailModel> BookDetails { get; set; }}
}
